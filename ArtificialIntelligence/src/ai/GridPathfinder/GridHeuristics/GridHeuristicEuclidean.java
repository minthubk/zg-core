package ai.GridPathfinder.GridHeuristics;

import ai.GridPathfinder.GridHeuristic;
import ai.GridPathfinder.GridNode;

public class GridHeuristicEuclidean implements GridHeuristic {
    @Override
    public float compute(GridNode from, GridNode to) {
	float x = to.getX() - from.getX();
	float y = to.getY() - from.getY();

	return (float) Math.sqrt(x * x + y * y);

    }

}
