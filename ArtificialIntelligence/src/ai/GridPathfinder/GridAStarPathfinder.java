package ai.GridPathfinder;

import ai.GridPathfinder.Neighborhood.GridNeighborhood;
import ai.GridPathfinder.Neighborhood.GridSquaredDiagonalNeighborhood;

public class GridAStarPathfinder extends GridPathFinder {

    GridMap map;
    GridHeuristic heuristic;
    GridOpenList open;
    GridVisitedMap visited;
    GridNeighborhood neighbors;
    float alpha = 0.5f;

    public GridAStarPathfinder(final GridMap map, final GridHeuristic heuristic) {
	this(map, heuristic, new GridSquaredDiagonalNeighborhood());
    }

    public GridAStarPathfinder(final GridMap map,
	    final GridHeuristic heuristic, final GridNeighborhood neighborhood) {
	this.map = map;
	this.heuristic = heuristic;
	this.neighbors = neighborhood;
	open = new GridOpenList();
	visited = new GridVisitedMap();
    }

    @Override
    public GridPath findPath(GridMover mover, GridNode start, GridNode end) {

	/* Avoid stupid waste of cpu */
	if (start.equalLocation(end))
	    return null;

	GridPath path = null;
	GridNode[] neighborhood;
	boolean endIsFound = false;

	/* Clean old data */
	open.clear();
	visited.clear();

	start.reset();

	/* Add starting position to search for */
	open.add(start);

	while (open.size() > 0) {

	    GridNode best = open.findBest();

	    /* Target reached */
	    if (best.equalLocation(end)) {
		endIsFound = true;
		break;
	    }

	    visited.put(best.hashCode(), best);

	    // Get neighborhood
	    neighborhood = neighbors.findNeighborhood(map, this, mover, best);

	    for (int i = 0; i < neighbors.size(); i++) {
		GridNode neighbor = neighborhood[i];

		// If node is not reachable from current node
		float gScore = best.g + map.getCost(mover, best, neighbor);
		if (gScore == Float.MAX_VALUE)
		    continue;

		// All nodes should be on visited map
		if (!visited.containsKey(neighbor.hashCode())) {
		    neighbor.reset();
		    neighbor.g = Float.MAX_VALUE;
		    visited.put(neighbor.hashCode(), neighbor);
		}

		if (best.g + gScore < neighbor.g) {
		    if (!open.contains(neighbor))
			open.add(neighbor);

		    neighbor.g = best.g + gScore;
		    neighbor.h = heuristic.compute(neighbor, end);
		    neighbor.f = (alpha * neighbor.g + (1 - alpha) * neighbor.h)
			    / Math.max(alpha, 1 - alpha);
		    neighbor.parent = best;
		}
	    }
	}

	if (endIsFound) {
	    path = new GridPath();
	    /* trace back path */
	    GridNode cursor = end;
	    do {
		path.prependStep(cursor.x, cursor.y, cursor.g);
		cursor = cursor.parent;
	    } while (!cursor.equalLocation(start) && (cursor.parent != null));
	    /* Add starting node */
	    path.prependStep(start.x, start.y);
	}

	return path;
    }

    @Override
    public boolean validNode(GridMover mover, int x, int y) {
	boolean result = true;
	result = (x >= 0) && (x < map.getWidth()) && (y >= 0)
		&& (y < map.getHeight());
	if (result) {
	    result = !map.isBlocked(mover, x, y);
	}
	return result;
    }
}
