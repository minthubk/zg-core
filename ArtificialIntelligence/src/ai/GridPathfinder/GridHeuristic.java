package ai.GridPathfinder;

public interface GridHeuristic {
    public float compute(GridNode from, GridNode to);
}
