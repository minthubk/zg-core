package ai.GridPathfinder;

public interface GridMap {

    public int getWidth();

    public int getHeight();

    public GridNode getNode(int x, int y);

    public boolean isBlocked(GridMover node, int x, int y);

    public float getCost(GridMover mover, GridNode from, GridNode to);

}
