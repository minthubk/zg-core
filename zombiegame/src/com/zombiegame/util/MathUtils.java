package com.zombiegame.util;

public class MathUtils {

	public static float distance(float pX1, float pY1, float pX2, float pY2) {
		final float dX = pX2 - pX1;
		final float dY = pY2 - pY1;
		return (float) Math.sqrt((dX * dX) + (dY * dY));
	}

	public static float abs(final float value) {
		if (value > 0.0f)
			return value;
		else
			return -value;
	}

	public static float sign(final float value) {
		if (value > 0.0f)
			return 1.0f;
		if (value < 0.0f)
			return -1.0f;
		return 0.0f;
	}
}
