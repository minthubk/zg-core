package com.zombiegame.logic.map;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.tiled.TileSet;
import com.badlogic.gdx.graphics.g2d.tiled.TiledLayer;
import com.badlogic.gdx.graphics.g2d.tiled.TiledMap;
import com.badlogic.gdx.graphics.g2d.tiled.TiledObject;
import com.badlogic.gdx.graphics.g2d.tiled.TiledObjectGroup;
import com.badlogic.gdx.utils.Json;
import com.zombiegame.logic.Ware;

public class RandomMap {
	private int seed;
	private String tileset;

	public RandomMap(String tileset, int seed) {
		this.tileset = tileset;
		this.seed = seed;
	}

	public int getSeed() {
		return seed;
	}

	public String getTileset() {
		return tileset;
	}

	private TiledLayer createTiledLayer() {
		TiledLayer tl = null;
		Constructor<TiledLayer> c;
		try {
			c = TiledLayer.class.getDeclaredConstructor();
			c.setAccessible(true);
			tl = c.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tl;
	}

	private TileSet createTileSet() {
		TileSet ts = null;
		Constructor<TileSet> c;
		try {
			c = TileSet.class.getDeclaredConstructor();
			c.setAccessible(true);
			ts = c.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ts;
	}

	public TiledMap generate(int width, int height) {
		TiledMap map = null;
		Random rand = new Random(seed);

		Json json = new Json();
		TilesetDefinition tsd = json.fromJson(TilesetDefinition.class,
				Gdx.files.internal(tileset));

		/* 1 - Create map object */
		map = new TiledMap();
		map.width = width;
		map.height = height;

		/* 2 - tileWidth and tileHeight loaded from tile set file */
		map.tileWidth = tsd.tileWidth;
		map.tileHeight = tsd.tileHeight;

		/* 3 - Setup tile properties */
		for (int i = 0; i < tsd.grounds.length; i++) {
			TileDefinition td = tsd.grounds[i];
			if ("grass".equals(td.name)) {
				for (int j = 0; j < td.width; j++)
					for (int k = 0; k < td.height; k++)
						map.setTileProperty(td.tiles[j][k], "Terrain", "grass");
			}
		}

		for (int i = 0; i < tsd.buildings.length; i++) {
			TileDefinition td = tsd.buildings[i];
			for (int j = 0; j < td.width; j++)
				for (int k = 0; k < td.height; k++)
					if (td.tiles[k][j] > 0)
						map.setTileProperty(td.tiles[k][j], "Terrain", "wall");
		}

		/* 4 - Setup map with tileset definitions */
		map.tileSets.add(createTileSet());
		map.tileSets.get(0).firstgid = 1;
		map.tileSets.get(0).imageName = tsd.fileGfx;
		map.tileSets.get(0).tileWidth = tsd.tileWidth;
		map.tileSets.get(0).tileHeight = tsd.tileHeight;

		/* 5 - Create layers and object group */
		TiledLayer ground = createTiledLayer();
		TiledLayer building = createTiledLayer();
		TiledLayer resource = createTiledLayer();
		TiledObjectGroup objects = new TiledObjectGroup();

		/* 6 - Setup layers and object group data */
		ground.name = "Ground";
		ground.tiles = new int[height][width];

		building.name = "Building";
		building.tiles = new int[height][width];

		resource.name = "Resource";
		resource.tiles = new int[height][width];

		objects.name = "Resources";
		TiledObject to = new TiledObject();
		to.properties.put("Resources", "true");
		objects.objects.add(to);

		/* 7 - Generate and populate ground layer */
		for (int j = 0; j < map.height; j++)
			for (int i = 0; i < map.width; i++) {
				ground.tiles[j][i] = tsd.grounds[0].tiles[j
						% tsd.grounds[0].height][i % tsd.grounds[0].width];
			}

		/* 8 - Generate and populate building layer */
		for (int i = 0; i < 10;) {
			if (placeRandomBuilding(rand, tsd, building)) {
				i++;
			}
		}

		/* 9 - Generate and populate resource layer */
		for (int i = 0; i < 20;) {
			if (placeRandomResource(rand, map, tsd, building, objects))
				i++;
		}

		map.layers.add(ground);
		map.layers.add(building);
		map.layers.add(resource);
		map.objectGroups.add(objects);
		return map;
	}

	boolean placeRandomBuilding(Random rand, TilesetDefinition tsd,
			TiledLayer layer) {
		int num = rand.nextInt(tsd.buildings.length);
		TileDefinition td = tsd.buildings[num];
		int x, y;

		x = rand.nextInt(layer.getWidth());
		y = rand.nextInt(layer.getHeight());

		if (isEmptySpace(layer, x, y, td.width, td.height)) {

			for (int i = 0; i < td.width; i++)
				for (int j = 0; j < td.height; j++)
					layer.tiles[y + j][x + i] = td.tiles[j][i];

			return true;
		}

		return false;
	}

	boolean placeRandomResource(Random rand, TiledMap map,
			TilesetDefinition tsd, TiledLayer ground, TiledObjectGroup objects) {

		int x, y;
		x = rand.nextInt(ground.getWidth());
		y = rand.nextInt(ground.getHeight());

		if (!isEmptySpace(ground, x, y, 1, 1))
			return false;

		int ware = rand.nextInt(Ware.Size - 1) + 1;
		int quantity = rand.nextInt(50) + rand.nextInt(20) * 5;

		TiledObject to;
		to = new TiledObject();
		to.x = x * map.tileWidth;
		to.y = y * map.tileHeight;
		to.width = map.tileWidth;
		to.height = map.tileHeight;
		to.properties.put(Ware.name[ware], quantity + "");

		objects.objects.add(to);

		return true;
	}

	private boolean isEmptySpace(TiledLayer layer, int x, int y, int width,
			int height) {
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++) {

				if ((x + i < 0) || (x + i >= layer.getWidth()) || (y + j < 0)
						|| (y + j >= layer.getHeight()))
					return false;

				if (layer.tiles[y + j][x + i] != 0)
					return false;
			}
		return true;
	}
}
