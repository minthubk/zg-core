package com.zombiegame.logic.map;

public class TilesetDefinition {
	String name;
	String fileGfx;
	int tileWidth;
	int tileHeight;
	TileDefinition[] grounds;
	TileDefinition[] walls;
	TileDefinition[] buildings;
}
