package com.zombiegame.logic;

import java.util.HashMap;
import java.util.Map;

public class Property {

	public enum GameObjectType {
		gotResource, gotSurvivor, gotBuilding, gotZombie
	}

	// Object name:
	// "survivor", "zombie", "warehouse", "barrier", "guntower"
	String name = "unknown";

	float movementSpeed = 32f;

	/* Health related */
	int maxHitPoints = 1;
	boolean canIgnoreHits = false;
	float ignoreHitsDelay = 1.0f;

	/* Is allowed to shoot : false by default */
	boolean canShot = false;
	/* Seconds delayed between shoots : 1.0f by default */
	float shotDelay = 1.0f;
	/* Range of weapon in tiles : 3 by default */
	int shotRange = 3;
	/* Ammo used */
	String shotAmmo = ""; // "bullet", "arrow" or null
	/* Shot noise */
	float shotNoise = 6f;

	/* Range of view in tiles */
	int rangeOfView = 3;

	/* Type of GameObject */
	GameObjectType type = GameObjectType.gotBuilding;

	/* Pack size when transporting goods : 10 by default */
	int packSize = 10;

	/* Building can supply other requests buildings : false by default */
	boolean supplyRequests = false;
	/* Seconds between supply dispatch : 1.0 by default */
	float supplyDelay = 1.0f;
	/* Supply radius, it only matters for warehouses: 9 by default */
	int supplyRadius = 9;

	/* Building is self dispatching his goods : true by default */
	boolean selfDispatcher = true;
	/* Building must dispatch goods as soon as possible : false by default */
	boolean mustDispatchGoods = false;
	/* Seconds between goods dispatching : 1.0f by default */
	float dispatchDelay = 1.0f;

	/* Economy specific */
	Map<String, Integer> creationCost;
	Map<String, Boolean> canStoreWare;
	String[] allowedWareControl = { Ware.name[Ware.survivor],
			Ware.name[Ware.wood], Ware.name[Ware.bullet] };
	String[] buildCost = {};
	/* When no resources on map tile and no resources on storage, removes */
	boolean destroyWhenNoResources = false;

	/* Gather specific */
	boolean canGather = false;
	float gatherTime = 1.0f;
	int gatherNoise = 1;

	public Property() {
		creationCost = new HashMap<String, Integer>();
		canStoreWare = new HashMap<String, Boolean>();
	}

	public int getMaxHitPoints() {
		return maxHitPoints;
	}

	public Property setMaxHitPoints(int newMax) {
		maxHitPoints = newMax;
		return this;
	}

	public boolean canIgnoreHits() {
		return canIgnoreHits;
	}

	public Property setCanIgnoreHits(boolean ignoreHits) {
		canIgnoreHits = ignoreHits;
		return this;
	}

	public float getIgnoreHitsDelay() {
		return ignoreHitsDelay;
	}

	public Property setIgnoreHitsDelay(float delay) {
		ignoreHitsDelay = delay;
		return this;
	}

	public boolean canShot() {
		return canShot;
	}

	public Property setCanShot(boolean allowed) {
		canShot = allowed;
		return this;
	}

	public float getShotDelay() {
		return shotDelay;
	}

	public Property setShotDelay(float delay) {
		shotDelay = delay;
		return this;
	}

	public String getName() {
		return name;
	}

	public Property setName(String name) {
		this.name = name;
		return this;
	}

	public int getShotRange() {
		return shotRange;
	}

	public Property setShotRange(int shotRange) {
		this.shotRange = shotRange;
		return this;
	}

	public String getShotAmmo() {
		return shotAmmo;
	}

	public Property setShotAmmo(String shotAmmo) {
		this.shotAmmo = shotAmmo;
		return this;
	}

	public int getRangeOfView() {
		return rangeOfView;
	}

	public Property setRangeOfView(int range) {
		this.rangeOfView = range;
		return this;
	}

	public GameObjectType gameObjectType() {
		return type;
	}

	public Property setGameObjectType(GameObjectType type) {
		this.type = type;
		return this;
	}

	public boolean canSupplyRequests() {
		return supplyRequests;
	}

	public Property setCanSupplyRequests(boolean canSupplyRequests) {
		this.supplyRequests = canSupplyRequests;
		return this;
	}

	public float getSupplyDelay() {
		return supplyDelay;
	}

	public Property setSupplyDelay(float supplyDelay) {
		this.supplyDelay = supplyDelay;
		return this;
	}

	public boolean isSelfDispather() {
		return selfDispatcher;
	}

	public Property setCanDispatchGoods(boolean allowed) {
		this.selfDispatcher = allowed;
		return this;
	}

	public boolean isMustDispatchGoods() {
		return mustDispatchGoods;
	}

	public Property setMustDispatchGoods(boolean mustDispatchGoods) {
		this.mustDispatchGoods = mustDispatchGoods;
		return this;
	}

	public float getDispatchDelay() {
		return dispatchDelay;
	}

	public Property setDispatchDelay(float dispatchDelay) {
		this.dispatchDelay = dispatchDelay;
		return this;
	}

	public Map<String, Integer> getCreationCost() {
		return creationCost;
	}

	public Property setCreationCost(Map<String, Integer> creationCost) {
		this.creationCost = creationCost;
		return this;
	}

	public Map<String, Boolean> getCanStoreWare() {
		return canStoreWare;
	}

	public Property setCanStoreWare(Map<String, Boolean> canStoreWare) {
		this.canStoreWare = canStoreWare;
		return this;
	}

	public boolean isDestroyWhenNoResources() {
		return destroyWhenNoResources;
	}

	public Property setDestroyWhenNoResources(boolean destroyWhenNoResources) {
		this.destroyWhenNoResources = destroyWhenNoResources;
		return this;
	}

	public boolean canGather() {
		return canGather;
	}

	public Property setCanGather(boolean canGather) {
		this.canGather = canGather;
		return this;
	}

	public float getGatherTime() {
		return gatherTime;
	}

	public Property setGatherTime(float gatherTime) {
		this.gatherTime = gatherTime;
		return this;
	}

	public int getPackSize() {
		return packSize;
	}

	public Property setPackSize(int packSize) {
		this.packSize = packSize;
		return this;
	}

	public float getMovementSpeed() {
		return movementSpeed;
	}

	public Property setMovementSpeed(float pixelspersecond) {
		this.movementSpeed = pixelspersecond;
		return this;
	}

	public String[] getAllowedWareControl() {
		return allowedWareControl;
	}

	public Property setAllowedWareControl(String[] allowed) {
		allowedWareControl = allowed.clone();
		return this;
	}

	public String[] getBuildCost() {
		return buildCost;
	}

	public Property setBuildCost(String[] cost) {
		buildCost = cost.clone();
		return this;
	}

	public float getShotNoise() {
		return shotNoise;
	}

	public int getSupplyRadius() {
		return supplyRadius;
	}
}
