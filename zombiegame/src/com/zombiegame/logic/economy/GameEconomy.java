package com.zombiegame.logic.economy;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.zombiegame.logic.Ware;

public class GameEconomy {

	/* Nodes of economy */
	ArrayList<EconomyNode> nodes;
	/* Ware stock on economy */
	int[] stock;
	/* Balanced stock in economy, here for speedup reasons */
	int[] balanced;

	public GameEconomy() {
		nodes = new ArrayList<EconomyNode>();
		stock = new int[Ware.Size];
		balanced = new int[Ware.Size];
	}

	public void addEconomyNode(EconomyNode node) {
		assert !nodes.contains(node);
		nodes.add(node);
		addStock(node);
	}

	/**
	 * @return true if economy split
	 */
	public boolean removeEconomyNode(EconomyNode node) {
		assert nodes.contains(node);

		if (nodes.remove(node))
			removeStock(node);

		return false;
	}

	private void addStock(EconomyNode node) {
		for (int i = Ware.Min; i < Ware.Size; i++)
			stock[i] += node.getStock(i);
	}

	private void removeStock(EconomyNode node) {
		for (int i = Ware.Min; i < Ware.Size; i++)
			stock[i] -= node.getStock(i);
	}

	public boolean inRange(EconomyNode node) {

		for (int i = 0; i < nodes.size(); i++)
			if (node.inRange(nodes.get(i)))
				return true;

		return false;
	}

	public boolean inRange(GameEconomy economy) {

		for (int i = 0; i < nodes.size(); i++)
			if (economy.inRange(nodes.get(i)))
				return true;

		return false;
	}

	private void computeStock() {
		for (int i = Ware.Min; i < Ware.Size; i++)
			computeStock(i);

	}

	private void computeStock(int ware) {
		Ware.checkValid(ware);
		stock[ware] = 0;
		for (int i = 0; i < nodes.size(); i++) {
			stock[ware] += nodes.get(i).getStock(ware);
		}
	}

	void updatePriorities() {

		/* Nothing to update if not enought nodes */
		if (nodes.size() < 2)
			return;

		/* Update stock in economy */
		computeStock();

		for (int ware = Ware.Min; ware < Ware.Size; ware++) {

			balanced[ware] = stock[ware] / nodes.size();

			// for (int i = 0; i < nodes.size(); i++) {
			// EconomyNode node = nodes.get(i);
			// node.setBalanced(ware, balanced[ware]);
			// }
			Gdx.app.log("WARE " + ware, "Balanced: " + balanced[ware]);
		}
	}

	public int[] getBalanced() {
		return balanced;
	}
}
