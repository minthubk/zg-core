package com.zombiegame.logic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.management.RuntimeErrorException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.tiled.TiledLoader;
import com.badlogic.gdx.graphics.g2d.tiled.TiledMap;
import com.badlogic.gdx.graphics.g2d.tiled.TiledObject;
import com.badlogic.gdx.graphics.g2d.tiled.TiledObjectGroup;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Json;
import com.zombiegame.logic.ai.AI_States;
import com.zombiegame.logic.economy.EconomyManager;
import com.zombiegame.logic.events.NeighborhoodManager;
import com.zombiegame.logic.events.WorldListener;
import com.zombiegame.logic.events.WorldNotifier;
import com.zombiegame.logic.gameobject.GameObject;
import com.zombiegame.logic.gameobject.GameObjectBuilding;
import com.zombiegame.logic.gameobject.GameObjectMapResource;
import com.zombiegame.logic.gameobject.GameObjectOwned;
import com.zombiegame.logic.gameobject.GameObjectSpawner;
import com.zombiegame.logic.gameobject.GameObjectSurvivor;
import com.zombiegame.logic.gameobject.GameObjectZombie;
import com.zombiegame.logic.goal.GoalManager;
import com.zombiegame.logic.map.RandomMap;
import com.zombiegame.pools.BuildingPool;
import com.zombiegame.pools.SurvivorPool;
import com.zombiegame.pools.ZombiePool;
import com.zombiegame.util.Assets;
import com.zombiegame.util.DummyGameReporter;
import com.zombiegame.util.GameReporter;

public class GameWorld implements Disposable {

	// ========================================================================
	// Constants
	// ========================================================================
	private static final String TAG = "GameWorld";

	/* Game states */
	public static final int UNITIALIZED = 0;
	public static final int LOADING = UNITIALIZED + 1;
	public static final int RUNNING = LOADING + 1;
	public static final int ONE_STEP = RUNNING + 1; /* One update, then pause */
	public static final int PAUSED = ONE_STEP + 1;
	public static final int LOSED = PAUSED + 1;
	public static final int WINNED = LOSED + 1;

	// ========================================================================
	// Fields
	// ========================================================================

	WorldNotifier notifier;

	// Map
	GameTiledMap map;

	// Object containers
	List<GameObjectZombie> mZombies;
	List<GameObjectSurvivor> mSurvivors;
	List<GameObjectBuilding> mBuildings;
	ArrayList<GameObjectSpawner> spawners;
	List<GameObjectOwned> garbage;

	// Pools
	SurvivorPool mSurvivorPool;
	ZombiePool mZombiePool;
	BuildingPool mBuildingPool;

	// Property manager
	PropertyContainerManager mGameObjectPropertyManager;
	GamePathfinder gamePathfinder;
	NeighborhoodManager neighborhoodHandler;
	MapResourceManager resourceManager;

	// Random manager
	Random mRandom;

	GameReporter reporter;
	GoalManager goals;
	EconomyManager economy;

	private int state;

	private int waveNumber;

	static GameWorld instance;

	public GameWorld(GameReporter gr) {

		long start, end;

		if (instance != null)
			throw new RuntimeErrorException(new Error("GameWorld"),
					"Only one instance is allowed");
		/* Always last instance */
		instance = this;

		start = System.currentTimeMillis();
		reporter = (gr == null ? new DummyGameReporter() : gr);

		// ==========================================================
		// Sample forced report
		// gr.sendReport(new GameRuntimeException("Message to log"));
		//
		// ==========================================================

		mZombies = new ArrayList<GameObjectZombie>();
		mSurvivors = new ArrayList<GameObjectSurvivor>();
		mBuildings = new ArrayList<GameObjectBuilding>();
		spawners = new ArrayList<GameObjectSpawner>();
		garbage = new ArrayList<GameObjectOwned>();

		mZombiePool = new ZombiePool();
		mBuildingPool = new BuildingPool();
		mSurvivorPool = new SurvivorPool();

		mGameObjectPropertyManager = new PropertyContainerManager();
		gamePathfinder = new GamePathfinder();
		neighborhoodHandler = new NeighborhoodManager();
		mRandom = new Random();

		notifier = new WorldNotifier();
		goals = new GoalManager();
		economy = new EconomyManager();

		resourceManager = new MapResourceManager();

		addWorldListener(gamePathfinder);
		addWorldListener(neighborhoodHandler);
		addWorldListener(goals);
		loadProperties();
		end = System.currentTimeMillis();

		Gdx.app.log(TAG, "Constructor timed " + (end - start) + "ms");
		state = UNITIALIZED;
	}

	public static GameWorld $() {
		if (instance == null)
			throw new RuntimeErrorException(new Error(
					"No GameWorld instance created"));
		return instance;
	}

	public void reset() {

		for (int i = 0; i < mBuildings.size(); i++)
			recycle(mBuildings.get(i));

		for (int i = 0; i < mSurvivors.size(); i++)
			recycle(mSurvivors.get(i));

		for (int i = 0; i < mZombies.size(); i++)
			recycle(mZombies.get(i));

		for (int i = 0; i < spawners.size(); i++)
			spawners.get(i).remove();

		doGarbageCollection();
		spawners.clear();

		state = UNITIALIZED;
		waveNumber = 0;
		goals = GoalManager.Default();
		// resourceManager.reset();
	}

	private void doGarbageCollection() {
		// Remove recycled
		while (garbage.size() > 0) {
			GameObjectOwned go = garbage.remove(garbage.size() - 1);

			if (go instanceof GameObjectSurvivor) {
				mSurvivors.remove(go);
				notifier.onGameObjectRemoved(go);
				mSurvivorPool.recyclePoolItem((GameObjectSurvivor) go);

			} else if (go instanceof GameObjectZombie) {
				mZombies.remove(go);
				notifier.onGameObjectRemoved(go);
				mZombiePool.recyclePoolItem((GameObjectZombie) go);

			} else if (go instanceof GameObjectBuilding) {
				mBuildings.remove(go);
				notifier.onGameObjectRemoved(go);
				mBuildingPool.recyclePoolItem((GameObjectBuilding) go);
			} else {
				Gdx.app.error(TAG, "Unkown gameobject descendant " + go);
			}
		}
	}

	public GameReporter getReporter() {
		return reporter;
	}

	public void addWorldListener(WorldListener listener) {
		notifier.addListener(listener);
	}

	public GameObjectBuilding createBuilding(int tile, String type) {
		return createBuilding(tile, type, true);
	}

	private GameObjectBuilding createBuilding(int tile, String type,
			boolean managed) {
		boolean allow = true;
		int x, y;
		y = tileToMapCoordY(tile);
		x = tileToMapCoordX(tile, y);

		allow = map.getTerrainType(x, y) == GameTiledMap.GRASS;

		if (type.equals(GameObjectBuilding.GATHERHOUSE))
			allow = allow && getResourceCountFromTile(tile) > 0;
		else
			allow = allow && getResourceCountFromTile(tile) == 0;

		if (getResources().hasZombies(tile))
			allow = false;

		if (allow) {
			GameObjectBuilding building = mBuildingPool.obtainPoolItem();

			placeOnTile(tile, building);
			building.setName(type);
			building.setup();

			if (managed) {
				mBuildings.add(building);
				notifier.onGameObjectPlaced(building);
			}

			return building;
		} else {
			return null;
		}

	}

	/**
	 * Create a survivor on map. It is added to mSurvivors array.
	 * 
	 * @param tile
	 *            where will be paced on
	 * @return the Survivor instance
	 */
	public GameObjectSurvivor createSurvivor(int tile) {
		return createSurvivor(tile, true);
	}

	/**
	 * Create a survivor on map
	 * 
	 * @param pTile
	 *            tile where will be placed on
	 * @param pManaged
	 *            if true, survivor will be added to mSurvivors
	 * @return the Survivor instance
	 */
	public GameObjectSurvivor createSurvivor(int tile, boolean managed) {
		GameObjectSurvivor survivor = mSurvivorPool.obtainPoolItem();
		survivor.setPathfinder(gamePathfinder.getPathfinder());

		placeOnTile(tile, survivor);

		if (managed)
			manage(survivor);

		return survivor;
	}

	public GameObjectZombie createZombie(int tile) {
		GameObjectZombie zombie = mZombiePool.obtainPoolItem();
		zombie.setPathfinder(gamePathfinder.getPathfinder());

		placeOnTile(tile, zombie);

		mZombies.add(zombie);
		notifier.onGameObjectPlaced(zombie);

		return zombie;
	}

	public GameObjectSpawner createSpawner(int tile) {
		GameObjectSpawner spawner = new GameObjectSpawner();

		placeOnTile(tile, spawner);

		spawners.add(spawner);

		return spawner;
	}

	@Override
	public void dispose() {
		mZombies.clear();
		mSurvivors.clear();
		mBuildings.clear();
		spawners.clear();
		mGameObjectPropertyManager.clear();

		goals = null;

		resourceManager.dispose();
	}

	public void generateTestData() {

		for (int i = 0; i < 25; i++) {
			int x, y;
			x = i;
			y = (map.getTiled().height - 1) - i;
			createZombie(x + y * map.getTiled().width);
		}

		// for (int i = 0; i < 10; i++) {
		// createSurvivor((int) (map.getTiled().width * Math.random())
		// * (int) (map.getTiled().height * Math.random()));
		// }

		// for (int i = 0; i < 20; i++) {
		// Building b = createBuilding((int) (mMap.width * Math.random())
		// * (int) (mMap.height * Math.random()));
		// b.setName(i % 2 == 0 ? "warehouse" : "warehouse");
		// }
		// Building b = createBuilding(4, Building.WAREHOUSE);
		// b.getStorage().increaseStored(Ware.wood, 100);
		// b.getStorage().increaseStored(Ware.bullet, 50);
		// b.getStorage().increaseDesiredGoods(Ware.wood);
		// b.getStorage().increaseDesiredGoods(Ware.bullet);
		//
		// b = createBuilding(0, Building.WAREHOUSE);
		// b.getStorage().increaseStored(Ware.wood, 0);
		// b.getStorage().increaseStored(Ware.bullet, 0);
		// b.getStorage().increaseDesiredGoods(Ware.wood);
		// b.getStorage().increaseDesiredGoods(Ware.bullet);

		createSurvivor(80);
		createSurvivor(180);
	}

	// ========================================================================
	// Getters
	// ========================================================================

	public List<GameObjectBuilding> getBuildings() {
		return mBuildings;
	}

	private enum IdName {
		SURVIVOR, ZOMBIE, WAREHOUSE, BARRIER
	}

	public int getIdFromName(String name) {
		IdName value = IdName.valueOf(name.toUpperCase());
		return value.ordinal();
	}

	public GameTiledMap getMap() {
		return map;
	}

	public Property getProperties(GameObjectOwned gameObject) {
		final Property prop = mGameObjectPropertyManager
				.objectProperties(gameObject);
		return prop;
	}

	public Random getRandom() {
		return mRandom;
	}

	public int getRandomResourceFromTile(int tile) {
		return resourceManager.randomResource(this, tile);
	}

	// ========================================================================
	// Update methods
	// ========================================================================

	public int getResourceCountFromTile(int tile) {
		return resourceManager.resourceCount(tile);
	}

	public List<GameObjectSurvivor> getSurvivors() {
		return mSurvivors;
	}

	public List<GameObjectZombie> getZombies() {
		return mZombies;
	}

	public void loadMap(String pMapName) {

		if (state > UNITIALIZED)
			throw new RuntimeException(
					"Could not load a map on already started game!");

		state = LOADING;
		long start, end;

		start = System.currentTimeMillis();
		map = new GameTiledMap();
		TiledMap tmap = null;

		if (false) {
			/* Load normally and print to output */
			tmap = TiledLoader.createMap(Gdx.files.internal(Assets.mapBaseDir
					+ "/" + pMapName + ".tmx"));
		} else {
			tmap = new RandomMap("data/json/grass.tileset", 12345).generate(32,
					32);
		}
		end = System.currentTimeMillis();
		Gdx.app.log(TAG, "Map loaded in " + (end - start) + "mS");
		start = end;

		map.setTiledMap(tmap);

		end = System.currentTimeMillis();
		Gdx.app.log(TAG, "Map loaded in " + (end - start) + "mS");

		resourceManager.init();

		// Load objects
		start = System.currentTimeMillis();
		for (int index = 0; index < tmap.objectGroups.size(); index++) {

			final TiledObjectGroup group = tmap.objectGroups.get(index);

			if (group.name.equals("Resources"))
				for (int j = 0; j < group.objects.size(); j++) {
					final TiledObject obj = group.objects.get(j);
					loadMapWares(tmap, obj);
				}

		}
		end = System.currentTimeMillis();
		Gdx.app.log(TAG, "Map resources loaded in " + (end - start) + "mS");

		notifier.onMapLoaded();
		goals = GoalManager.Default();

	}

	private void loadMapWares(TiledMap tmap, TiledObject obj) {
		int x, y, c, r;
		// Init in columns and rows
		x = obj.x / tmap.tileWidth;
		y = obj.y / tmap.tileHeight;
		// Size in columns and row
		c = obj.width / tmap.tileWidth;
		r = obj.height / tmap.tileHeight;

		for (int tileRow = 0; tileRow < r; tileRow++) {
			for (int tileCol = 0; tileCol < c; tileCol++) {
				final int tileId = x + tileCol + (y + tileRow) * tmap.width;

				// Create resources
				final Set<String> stringSet = obj.properties.keySet();

				Iterator<String> it = stringSet.iterator();
				while (it.hasNext()) {
					final String realKey = it.next();
					final String strKey = realKey.toLowerCase();
					final int key = Ware.valueOf(strKey);

					if (key > 0) {
						int q = Integer.parseInt(obj.properties.get(realKey));
						resourceManager.setResource(tileId, key, q);
					}
				}
			}
		}
	}

	public void loadProperties() {

		Json reader = new Json();
		mGameObjectPropertyManager.clear();
		mGameObjectPropertyManager = reader.fromJson(
				PropertyContainerManager.class,
				Gdx.files.internal("data/json/data.json"));

		Gdx.app.log(TAG, reader.toJson(mGameObjectPropertyManager,
				PropertyContainerManager.class));

		// TODO Parse some properties to game constants
		// TODO Basically group resources and buildings
	}

	/**
	 * Adds Survivor to managed array. Prevents duplication
	 * 
	 * @param survivor
	 */
	public void manage(GameObjectSurvivor survivor) {
		if (mSurvivors.contains(survivor))
			return;

		mSurvivors.add(survivor);
		notifier.onGameObjectPlaced(survivor);

	}

	private void mapCoordsToWorldCoords(int x, int y, Vector2 out) {
		if (x < 0 || x >= map.getTiled().width)
			throw new IllegalArgumentException("X out of bounds!");

		if (y < 0 || y >= map.getTiled().height)
			throw new IllegalArgumentException("Y out of bounds!");

		out.x = map.getTiled().tileWidth * x;
		out.y = map.getTiled().tileHeight * (map.getTiled().height - 1 - y);
	}

	public void placeOnTile(int tile, GameObject gameObject) {
		tileToWorld(tile, gameObject.getPosition());
		gameObject.setPosition(gameObject.getPosition().x,
				gameObject.getPosition().y);
	}

	private void tileToWorld(int tile, Vector2 position) {
		int col, row;
		row = tileToMapCoordY(tile);
		col = tileToMapCoordX(tile, row);
		mapCoordsToWorldCoords(col, row, position);
	}

	public int tileToMapCoordX(int tile, int row) {
		return tile - row * map.getTiled().width;
	}

	public int tileToMapCoordY(int tile) {
		return tile / map.getTiled().width;
	}

	/**
	 * Converts world coordinates to map coordinates. Really returns tile on
	 * passed position.
	 * 
	 * @param x
	 *            position on world
	 * @param y
	 *            position on world
	 * @return tile index on map
	 */
	public int worldToTile(float x, float y) {
		final TiledMap m = map.getTiled();
		final int col = (int) (x / m.tileWidth);
		final int row = (int) (m.height - 1 - (y / m.tileHeight));
		return col + row * m.width;
	}

	public void update(float pDelta) {
		if (state != RUNNING && state != ONE_STEP)
			return;

		if (pDelta > 1 / 30f)
			pDelta = 1 / 30f;

		updateBuildings(pDelta);
		updateZombies(pDelta);
		updateSurvivors(pDelta);
		updateSpawners(pDelta);

		doGarbageCollection();

		goals.update(this, pDelta);

		if (goals.isGameLost())
			Gdx.app.log(TAG, "GAME OVER");

		if (goals.isGameWinned())
			Gdx.app.log(TAG, "WINER");

		if (state == ONE_STEP)
			state = PAUSED;

		economy.compute(pDelta);
	}

	private void updateBuildings(float pDelta) {

		for (int index = 0; index < mBuildings.size(); index++) {
			GameObjectBuilding pBuilding = mBuildings.get(index);
			pBuilding.update(pDelta);
		}

	}

	private void updateSurvivors(float pDelta) {

		for (int index = 0; index < mSurvivors.size(); index++) {
			GameObjectSurvivor pSurvivor = mSurvivors.get(index);
			pSurvivor.update(pDelta);
		}

	}

	private void updateZombies(float pDelta) {

		for (int index = 0; index < mZombies.size(); index++) {
			GameObjectZombie pZombie = mZombies.get(index);
			pZombie.update(pDelta);
		}

	}

	private void updateSpawners(float pDelta) {
		for (int index = spawners.size() - 1; index >= 0; index--) {
			if (spawners.get(index).isOutOfGame()) {
				spawners.remove(index);
			}
		}
	}

	public void recycle(GameObjectOwned object) {
		garbage.add(object);
	}

	public WorldNotifier getNotifier() {
		return notifier;
	}

	public GameObjectOwned getGameObjectAt(Vector3 position) {

		for (int i = 0; i < mBuildings.size(); i++) {
			GameObjectBuilding b = mBuildings.get(i);

			if (b.hit(position.x, position.y))
				return b;
		}

		for (int i = 0; i < mZombies.size(); i++) {
			GameObjectZombie z = mZombies.get(i);
			if (z.hit(position.x, position.y))
				return z;
		}

		for (int i = 0; i < mSurvivors.size(); i++) {
			GameObjectSurvivor s = mSurvivors.get(i);
			if (s.hit(position.x, position.y))
				return s;
		}

		return null;
	}

	public boolean isEnded() {
		return goals.isGameLost() || goals.isGameWinned();
	}

	public void start() {
		if (state == LOADING) {
			state = RUNNING;
			notifier.onResumed();
		}
	}

	public void resume() {
		if (state == PAUSED) {
			state = RUNNING;
			notifier.onResumed();
		}
	}

	public void pause() {
		if (state == RUNNING) {
			state = PAUSED;
			notifier.onPaused();
		}
	}

	public boolean isPaused() {
		return state == PAUSED;
	}

	public void launchWave() {
		waveNumber++;
		int numZombies = (mRandom.nextInt(3) + 2 + waveNumber) * 2;

		GameObjectZombie z;
		GameObjectOwned go;

		if (mBuildings.size() > 0)
			go = mBuildings.get(0);
		else
			go = mSurvivors.get(0);

		GameObjectMapResource mrt;
		for (int i = 0, j; i < numZombies; i++) {
			j = mRandom.nextInt(resourceManager.tiles.size());

			mrt = resourceManager.tiles.get(j);

			if (mrt.hasZombies()) {
				mrt.extractResource(Ware.none);
				z = createZombie(mrt.getTile());
			} else {
				if (spawners.size() < 1)
					continue;

				final GameObjectSpawner gos = spawners.get(mRandom
						.nextInt(spawners.size()));
				final int tile = worldToTile(gos.getPosition().x,
						gos.getPosition().y);
				z = createZombie(tile);
			}
			z.startState(AI_States.zAlert, go, go.getTile());
		}

	}

	public void dropResource(int tile, int ware, int value) {
		resourceManager.setResource(tile, ware, value);
	}

	public MapResourceManager getResources() {
		return resourceManager;
	}

	public void makeNoise(GameObjectOwned who, float power) {
		for (int i = 0; i < mZombies.size(); i++) {
			final GameObjectZombie z = mZombies.get(i);
			if (z.isAlive()) {
				z.hearNoise(who, power);
			}
		}
	}

	public boolean isMapLoaded() {
		return state > UNITIALIZED;
	}

	public EconomyManager getEconomyManager() {
		return economy;
	}

}
