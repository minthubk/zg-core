package com.zombiegame.logic.ai;

import com.zombiegame.logic.gameobject.GameObjectOwned;

public interface IGameObjectState {
	public boolean start(GameObjectOwned go);

	public void onEnter(GameObjectOwned go);

	public void onLeave(GameObjectOwned go);

	public void onUpdate(GameObjectOwned go);

	public void onMessage(GameObjectOwned go, GameObjectMessage msg);

}
