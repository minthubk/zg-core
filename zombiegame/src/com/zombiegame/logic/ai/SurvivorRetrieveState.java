package com.zombiegame.logic.ai;

import com.badlogic.gdx.Gdx;
import com.zombiegame.logic.Ware;
import com.zombiegame.logic.gameobject.GameObjectOwned;
import com.zombiegame.logic.gameobject.GameObjectSurvivor;

public class SurvivorRetrieveState extends DummyState {

	private static final String TAG = "SurvivorRetrieveState";

	@Override
	public boolean start(GameObjectOwned go) {
		Gdx.app.log(TAG, "start");
		boolean result = super.start(go);
		if (result) {
			try {
				GameObjectSurvivor s = (GameObjectSurvivor) go;
				result = s.walkToTile(go.target.getTile());
			} catch (NullPointerException e) {
				throw new NullPointerException(
						"Target is null on Retrieve state");
			}
		}
		return result;
	}

	@Override
	public void onEnter(GameObjectOwned go) {
		super.onEnter(go);
		Gdx.app.log(TAG, "onEnter");
		GameObjectSurvivor s = (GameObjectSurvivor) go;
		if (s.dataState instanceof Ware)
			s.setWare((Integer) s.dataState, 0);
		else
			throw new IllegalAccessError("dataState must be instanceof Ware");
	}

	@Override
	public void onLeave(GameObjectOwned go) {
		Gdx.app.log(TAG, "onLeave");
		super.onLeave(go);
	}

	@Override
	public void onUpdate(GameObjectOwned go) {

		if (go.canSeeEnemy()) {
			go.startState(AI_States.RunAway);
			return;
		}

		GameObjectSurvivor s = (GameObjectSurvivor) go;
		if (!s.isWalking()) {
			if (!s.hasGoods())
				s.loadGoods();
			else
				s.startState(AI_States.Return);
		}
	}

	@Override
	public void onMessage(GameObjectOwned go, GameObjectMessage msg) {
		// TODO Auto-generated method stub
	}

}
