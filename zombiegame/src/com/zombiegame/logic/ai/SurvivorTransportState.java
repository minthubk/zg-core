package com.zombiegame.logic.ai;

import com.zombiegame.logic.gameobject.GameObjectOwned;
import com.zombiegame.logic.gameobject.GameObjectSurvivor;

public class SurvivorTransportState extends DummyState {

	private static final String TAG = "SurvivorTransportState";

	@Override
	public boolean start(GameObjectOwned go) {
		boolean result = super.start(go);
		if (result) {
			try {
				GameObjectSurvivor s = (GameObjectSurvivor) go;
				result = s.walkToTile(s.target.getTile());
				if (result)
					s.target.getNotifier().addListener(s);
			} catch (NullPointerException e) {
				throw new RuntimeException(TAG + "Target is null");
			}
		}
		return result;
	}

	@Override
	public void onEnter(GameObjectOwned go) {
		super.onEnter(go);
	}

	@Override
	public void onLeave(GameObjectOwned go) {
		super.onLeave(go);
	}

	@Override
	public void onUpdate(GameObjectOwned go) {

		if (go.canSeeEnemy()) {
			go.startState(AI_States.RunAway);
			return;
		}

		GameObjectSurvivor s = (GameObjectSurvivor) go;
		if (!s.isWalking()) {
			if (s.hasTarget() && s.hasGoods())
				s.unloadGoods();
			else if (!s.startState(AI_States.Return))
				s.startState(AI_States.Idle);
		}
	}

	@Override
	public void onMessage(GameObjectOwned go, GameObjectMessage msg) {
		// TODO Auto-generated method stub
	}

}
