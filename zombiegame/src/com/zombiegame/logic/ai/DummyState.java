package com.zombiegame.logic.ai;

import com.zombiegame.logic.gameobject.GameObjectOwned;

public class DummyState implements IGameObjectState {

	public static IGameObjectState Dummy = new DummyState();

	@Override
	public boolean start(GameObjectOwned go) {
		// Dummy state always start sucessfully
		return true;
	}

	@Override
	public void onEnter(GameObjectOwned go) {
	}

	@Override
	public void onLeave(GameObjectOwned go) {
	}

	@Override
	public void onUpdate(GameObjectOwned go) {
		// Gdx.app.log("DummyState", "onUpdate()");
	}

	@Override
	public void onMessage(GameObjectOwned go, GameObjectMessage msg) {
		// Gdx.app.log("DummyState", "onMessage()");
	}

}
