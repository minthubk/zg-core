package com.zombiegame.logic.ai;

import com.zombiegame.logic.gameobject.GameObjectOwned;
import com.zombiegame.logic.gameobject.GameObjectSurvivor;

public class SurvivorIdleState extends DummyState {

	// private static final String TAG = "SurvivorIdleState";

	@Override
	public boolean start(GameObjectOwned go) {
		go.setAnimation(GameObjectOwned.ANIMATION_IDLE);
		return super.start(go);
	}

	@Override
	public void onEnter(GameObjectOwned go) {
		super.onEnter(go);
	}

	@Override
	public void onLeave(GameObjectOwned go) {
		super.onLeave(go);
	}

	@Override
	public void onUpdate(GameObjectOwned go) {

		if (go.canSeeEnemy()) {
			go.startState(AI_States.RunAway);
			return;
		}

		GameObjectSurvivor s = (GameObjectSurvivor) go;

		if (s.hasHome())
			s.startState(AI_States.Return);
		else
			s.findHome();

	}

	@Override
	public void onMessage(GameObjectOwned go, GameObjectMessage msg) {
		// TODO Auto-generated method stub
	}

}
