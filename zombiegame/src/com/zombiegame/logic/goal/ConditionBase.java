package com.zombiegame.logic.goal;

import com.zombiegame.logic.GameWorld;
import com.zombiegame.logic.events.WorldListener;
import com.zombiegame.logic.gameobject.GameObjectOwned;

public abstract class ConditionBase implements WorldListener {
	boolean completed;
	boolean visible;
	float nextCheck;
	float checkDelay;
	String text;

	Action action;

	public ConditionBase() {
		nextCheck = 1f;
		checkDelay = 1f;
	}

	public boolean isCompleted() {
		return completed;
	}

	public boolean isVisible() {
		return visible;
	}

	public final void check(GameWorld world, float delta) {
		if (nextCheck < 0f) {
			nextCheck += checkDelay;
			onCheck(world, delta);
			if (completed)
				doAction(world);
		}
		nextCheck -= delta;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	private void doAction(GameWorld world) {
		if (action != null)
			action.execute(world);
	}

	abstract void onCheck(GameWorld world, float delta);

	@Override
	public void onMapLoaded() {
	}

	@Override
	public void onGameObjectPlaced(GameObjectOwned object) {
	}

	@Override
	public void onGameObjectRemoved(GameObjectOwned object) {
	}

	@Override
	public void onGameObjectStartDie(GameObjectOwned object) {
	}

	@Override
	public void onGameObjectReserveMapPosition(GameObjectOwned go, int col,
			int row) {
	}

	@Override
	public void onGameObjectLeaveMapPosition(GameObjectOwned go, int col,
			int row) {
	}

	@Override
	public void onPaused() {
	}

	@Override
	public void onResumed() {
	}
}
