package com.zombiegame.logic.goal;

import java.util.List;

import com.zombiegame.logic.GameWorld;
import com.zombiegame.logic.gameobject.GameObjectBuilding;

public class ConditionSurvivorCount extends ConditionBase {

	int min, max;

	@Override
	void onCheck(GameWorld world, float delta) {
		int count = 0;

		count = world.getSurvivors().size();

		List<GameObjectBuilding> lb = world.getBuildings();
		for (int i = 0; i < lb.size(); i++) {
			count += lb.get(i).getInsideSurvivorsCount();
		}

		completed = (count >= min) && (count <= max);
	}

	public void setCount(int c) {
		setBounds(c, c);
	}

	public void setBounds(int min, int max) {
		this.min = min;
		this.max = max;
	}
}
