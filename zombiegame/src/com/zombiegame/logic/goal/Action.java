package com.zombiegame.logic.goal;

import com.zombiegame.logic.GameWorld;

public abstract class Action {

	int timesExecuted;

	public final void execute(GameWorld world) {
		timesExecuted++;
		onExecute(world);
	}

	abstract void onExecute(GameWorld world);
}
