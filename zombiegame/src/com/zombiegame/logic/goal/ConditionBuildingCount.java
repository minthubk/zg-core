package com.zombiegame.logic.goal;

import com.zombiegame.logic.GameWorld;

public class ConditionBuildingCount extends ConditionBase {

	int min, max;

	@Override
	void onCheck(GameWorld world, float delta) {
		int count = 0;

		count = world.getBuildings().size();

		completed = (count >= min) && (count <= max);
	}

	public void setCount(int c) {
		setBounds(c, c);
	}

	public void setBounds(int min, int max) {
		this.min = min;
		this.max = max;
	}
}
