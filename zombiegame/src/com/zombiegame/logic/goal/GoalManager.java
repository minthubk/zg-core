package com.zombiegame.logic.goal;

import java.util.ArrayList;

import com.zombiegame.logic.GameWorld;
import com.zombiegame.logic.events.WorldListener;
import com.zombiegame.logic.gameobject.GameObjectOwned;
import com.zombiegame.logic.goal.ConditionComposite.Operation;

public class GoalManager implements WorldListener {

	ArrayList<ConditionBase> victory;
	ArrayList<ConditionBase> defeat;
	ArrayList<ConditionBase> other;

	public GoalManager() {
		victory = new ArrayList<ConditionBase>();
		defeat = new ArrayList<ConditionBase>();
		other = new ArrayList<ConditionBase>();
	}

	public boolean isGameWinned() {
		for (int i = 0; i < victory.size(); i++) {
			if (!victory.get(i).isCompleted())
				return false;
		}
		return true;
	}

	public boolean isGameLost() {
		for (int i = 0; i < defeat.size(); i++) {
			if (!defeat.get(i).isCompleted())
				return false;
		}
		return true;
	}

	public void update(GameWorld world, float delta) {

		for (int i = 0; i < victory.size(); i++)
			victory.get(i).check(world, delta);

		for (int i = 0; i < defeat.size(); i++)
			defeat.get(i).check(world, delta);

		for (int i = 0; i < other.size(); i++)
			other.get(i).check(world, delta);
	}

	@Override
	public void onMapLoaded() {
		for (int i = 0; i < victory.size(); i++)
			victory.get(i).onMapLoaded();

		for (int i = 0; i < defeat.size(); i++)
			defeat.get(i).onMapLoaded();

		for (int i = 0; i < other.size(); i++)
			other.get(i).onMapLoaded();

	}

	@Override
	public void onGameObjectPlaced(GameObjectOwned object) {
		for (int i = 0; i < victory.size(); i++)
			victory.get(i).onGameObjectPlaced(object);

		for (int i = 0; i < defeat.size(); i++)
			defeat.get(i).onGameObjectPlaced(object);

		for (int i = 0; i < other.size(); i++)
			other.get(i).onGameObjectPlaced(object);

	}

	@Override
	public void onGameObjectRemoved(GameObjectOwned object) {
		for (int i = 0; i < victory.size(); i++)
			victory.get(i).onGameObjectRemoved(object);

		for (int i = 0; i < defeat.size(); i++)
			defeat.get(i).onGameObjectRemoved(object);

		for (int i = 0; i < other.size(); i++)
			other.get(i).onGameObjectRemoved(object);

	}

	@Override
	public void onGameObjectStartDie(GameObjectOwned object) {
		for (int i = 0; i < victory.size(); i++)
			victory.get(i).onGameObjectStartDie(object);

		for (int i = 0; i < defeat.size(); i++)
			defeat.get(i).onGameObjectStartDie(object);

		for (int i = 0; i < other.size(); i++)
			other.get(i).onGameObjectStartDie(object);

	}

	@Override
	public void onGameObjectReserveMapPosition(GameObjectOwned go, int col,
			int row) {
		// for (int i = 0; i < victory.size(); i++)
		// victory.get(i).onGameObjectReserveMapPosition(go, col, row);
		//
		// for (int i = 0; i < defeat.size(); i++)
		// defeat.get(i).onGameObjectReserveMapPosition(go, col, row);
	}

	@Override
	public void onGameObjectLeaveMapPosition(GameObjectOwned go, int col,
			int row) {
		// for (int i = 0; i < victory.size(); i++)
		// victory.get(i).onGameObjectLeaveMapPosition(go, col, row);
		//
		// for (int i = 0; i < defeat.size(); i++)
		// defeat.get(i).onGameObjectLeaveMapPosition(go, col, row);
	}

	@Override
	public void onPaused() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onResumed() {
		// TODO Auto-generated method stub
	}

	public void reset() {
		// TODO Auto-generated method stub
	}

	public static GoalManager Default() {
		GoalManager gm = new GoalManager();

		/* Standard victory conditions */
		gm.addVictory(new ConditionNoZombiesOnMap());

		/* Standard defeat conditions */
		ConditionComposite cb;
		ConditionBase ct;

		cb = new ConditionComposite();

		ct = new ConditionBuildingCount();
		((ConditionBuildingCount) ct).setCount(0);
		cb.setFirst(ct);

		ct = new ConditionSurvivorCount();
		((ConditionSurvivorCount) ct).setCount(0);
		cb.setSecond(ct);
		cb.setSecond(ct);

		cb.setOperation(Operation.opAnd);
		gm.addDefeat(cb);

		/* Standard general events */
		ConditionTimeElapsed cte = new ConditionTimeElapsed();
		cte.setup(180, true);
		cte.setAction(new ActionLaunchWave());
		gm.addOther(cte);
		return gm;
	}

	private void addVictory(ConditionBase base) {
		victory.add(base);
	}

	private void addDefeat(ConditionBase base) {
		defeat.add(base);
	}

	private void addOther(ConditionBase base) {
		other.add(base);
	}
}
