package com.zombiegame.logic.modules;

import java.util.ArrayList;
import java.util.Collections;
import java.util.ListIterator;

import com.badlogic.gdx.Gdx;
import com.zombiegame.logic.GameWorld;
import com.zombiegame.logic.Request;
import com.zombiegame.logic.Ware;
import com.zombiegame.logic.WareStorage;
import com.zombiegame.logic.ai.AI_States;
import com.zombiegame.logic.gameobject.GameObjectBuilding;
import com.zombiegame.logic.gameobject.GameObjectSurvivor;

public class Storage extends BuildingModule {

	// ========================================================================
	// Constants
	// ========================================================================

	private final static String TAG = "Storage";

	// ========================================================================
	// Fields
	// ========================================================================

	ArrayList<GameObjectSurvivor> mCarriers;
	Request[] mRequests; // total quantity, ignores Carrier
	boolean mReady;
	float mDispatchDelay;
	int[] mStored;
	int[] mCarried; // by some survivor to here
	int[] mDesired; // total desired quantity
	int[] mDesiredLess; // Priority for automatic resource spread

	ArrayList<Request> mTemp; // Temporal requests

	// ========================================================================
	// Methods
	// ========================================================================

	public Storage(GameObjectBuilding building) {
		super(building);
		mCarriers = new ArrayList<GameObjectSurvivor>();
		mStored = new int[Ware.Size];
		mCarried = new int[Ware.Size];
		mDesired = new int[Ware.Size];
		mDesiredLess = new int[Ware.Size];
		mRequests = new Request[Ware.Size];
		mTemp = new ArrayList<Request>();
		mReady = true;

		for (int i = 1; i < Ware.Size; i++) {
			mRequests[i] = new Request();
			mRequests[i].setSource(mBuilding);
			Gdx.app.log(TAG, "Setting request source to " + mBuilding + " = "
					+ mRequests[i].getSource());
		}
	}

	/**
	 * This is called by carrier when is killed or panic
	 * 
	 * @param survivor
	 */
	public void carrierLost(final GameObjectSurvivor survivor) {
		if (mCarriers.contains(survivor)) {
			mCarriers.remove(survivor);
		}

		// Reduce carried goods
		if (survivor.hasGoods()) {
			if (survivor.getWare().key != Ware.none) {
				reduceCarried(survivor.getWare());
				updateRequest(survivor.getWare().key);
			}
		}
	}

	/**
	 * This should be called before survivor going to sleep inside house. This
	 * just gets updated mDesired and mCarried data.
	 * 
	 * @param survivor
	 */
	public void carrierArrived(final GameObjectSurvivor survivor) {
		mCarriers.remove(survivor);

		// Add goods
		if (survivor.hasGoods()) {
			if (survivor.getWare().key != Ware.none) {
				increaseStored(survivor.getWare());
				reduceCarried(survivor.getWare());
				updateRequest(survivor.getWare().key);
				survivor.setWare(Ware.none, 0);
				mBuilding.getNotifier().notifyUpdated(mBuilding);
			}
		}
	}

	public int getDesired(final int key) {
		return mDesired[key];
	}

	public int getCarried(final int key) {
		return mCarried[key];
	}

	public int getStored(final int ware) {
		return mStored[ware];
	}

	public boolean increaseDesiredGoods(final int ware) {
		increaseDesired(ware, Ware.pkgSize(ware));
		return true;
	}

	public boolean decreaseDesiredGoods(final int ware) {
		decreaseDesired(ware, Ware.pkgSize(ware));
		return true;
	}

	// ================================
	// Override
	// ================================

	@Override
	protected void onClean() {

		for (int i = 1; i < Ware.Size; i++) {
			mRequests[i].initialize(i, 0);
			mStored[i] = 0;
			mDesired[i] = 0;
			mCarried[i] = 0;
		}

		while (mCarriers.size() > 0) {
			GameObjectSurvivor s = mCarriers.remove(mCarriers.size() - 1);
			s.notifyRemoved(mBuilding);
		}
	}

	@Override
	protected void onDestroy() {
		for (int i = 1; i < Ware.Size; i++) {
			mRequests[i] = null;
		}
		mStored = null;
		mDesired = null;
		mCarried = null;
		mCarriers = null;
		mRequests = null;
	}

	@Override
	protected void onWork(final float pSecondsElapsed) {

		updateRequests();

		if (mReady) {

			if (mBuilding.properties().canSupplyRequests()) {

				boolean hasSupplied = supplyGoods();

				if (!hasSupplied) {
					hasSupplied = sendCarrierToGet();
				}

				if (hasSupplied) {
					Gdx.app.log(TAG, "Supplied!");
					mReady = false;
					mDispatchDelay = mBuilding.properties().getSupplyDelay();
				}
			}

			/**
			 * Dispatch non requested goods (mainly for harvest house)
			 */
			if (mReady && mBuilding.properties().isMustDispatchGoods()) {
				if (dispatchGoods()) {
					mReady = false;
					mDispatchDelay = mBuilding.properties().getDispatchDelay();
				}
			}

			/**
			 * Dispatch non requested goods between warehouses
			 */
			if (mReady && mBuilding.properties().canSupplyRequests()) {
				if (dispatchEconomyDesired()) {
					Gdx.app.log(TAG, "Spreading wares");
					mReady = false;
					mDispatchDelay = mBuilding.properties().getSupplyDelay();
				}
			}

		} else {
			mDispatchDelay -= pSecondsElapsed;
			if (mDispatchDelay < 0) {
				mReady = true;
			}
		}
	}

	// ================================
	// Helpers
	// ================================

	private boolean supplyGoods() {

		if (mBuilding.getInsideSurvivorsCount() > 0) {

			mTemp.clear();

			final ArrayList<GameObjectBuilding> neighborhood = mBuilding
					.getNeighborhood();

			if (neighborhood.size() == 0) {
				return false;
			}

			// Generate temporal array
			for (int index = 0; index < neighborhood.size(); index++) {
				final Storage other = neighborhood.get(index).getStorage();
				for (int i = 1; i < Ware.Size; i++)
					mTemp.add(other.mRequests[i]);
			}

			// Sort by priority, reversed so higher first
			Collections.sort(mTemp, Collections.reverseOrder());

			// Now try to supply
			if (mBuilding.getInsideSurvivorsCount() > 0)
				while (!mTemp.isEmpty()) {

					final Request rCurrent = mTemp.remove(0);
					final WareStorage sWare = rCurrent.getWare();
					final GameObjectBuilding target = rCurrent.getSource();
					final int iPackage = Ware.pkgSize(sWare.key);

					// Ignore 0-sized requests
					if (sWare.value < 1)
						continue;

					if (mStored[sWare.key] >= iPackage) {

						final GameObjectSurvivor survivor = mBuilding
								.getInsideSurvivor();

						sendSurvivor(survivor, target, sWare.key, iPackage);

						return true;
					}
				}

		}
		return false;
	}

	/*
	 * Special function that search and send carriers to get packed resources
	 */
	private boolean sendCarrierToGet() {

		if (mBuilding.getInsideSurvivorsCount() > 0) {
			mTemp.clear();

			final ArrayList<GameObjectBuilding> neighborhood = mBuilding
					.getNeighborhood();

			// Add requests
			for (ListIterator<GameObjectBuilding> cIterator = neighborhood
					.listIterator(); cIterator.hasNext();) {

				final GameObjectBuilding b = cIterator.next();

				// Ignore self-sufficient buildings
				if (b.properties().isSelfDispather())
					continue;

				if (!b.properties().getCanStoreWare().containsKey("survivor")
						|| (b.properties().getCanStoreWare()
								.containsKey("survivor") && !b.properties()
								.getCanStoreWare().get("survivor")
								.booleanValue())) {

					for (int i = 1; i < Ware.Size; i++) {
						final int cachedWare = i;
						// To save time later, only add needed requests
						if ((b.getStorage().getDesired(cachedWare) + b
								.getStorage().getCarried(cachedWare)) < b
								.getStorage().getStored(cachedWare)) {
							mTemp.add(b.getStorage().mRequests[i]);
						}
					}
				}
			}

			// Sort by priority
			Collections.sort(mTemp);

			if (!mTemp.isEmpty()) {
				final Request cRequest = mTemp.get(0);
				final GameObjectBuilding bDestiny = cRequest.getSource();
				final GameObjectSurvivor survivor = mBuilding
						.getInsideSurvivor();

				if (survivor != null) {
					if (survivor.startState(AI_States.Retrieve, bDestiny,
							cRequest.getWare().key)) {
						mBuilding.uncache(survivor);
					}
					return true;
				}
			}
		}

		return false;
	}

	private boolean dispatchGoods() {

		final GameObjectBuilding bWarehouse = mBuilding.nearestWarehouse();

		if (bWarehouse == null)
			return false;

		/**
		 * Forced to dispatch when no more resources to mine. It's a must to
		 * check against canGather to prevent barriers and sniper towers being
		 * destroyed
		 */
		boolean forceDispatch;

		if (mBuilding.properties().canGather()) {
			forceDispatch = mBuilding.getHarverster().isEmpty();
		} else {
			forceDispatch = true;
			for (int index = 1; index < Ware.Size; index++)
				forceDispatch = forceDispatch && (mDesired[index] == 0);
		}

		if (mBuilding.getInsideSurvivorsCount() < 1)
			return false;

		final GameObjectSurvivor survivor = mBuilding.getInsideSurvivor();

		/**
		 * TODO Better to store last dispatched and start a round robin around
		 * it
		 */
		for (int ware = Ware.Size - 1; ware > 0; ware--) {

			final int iPackage = Ware.pkgSize(ware);
			final boolean hasWares = mStored[ware] > 0;
			final boolean isPackageFilled = (mStored[ware] - mDesired[ware]) >= iPackage;

			if ((hasWares && forceDispatch) || isPackageFilled) {

				int quantity = iPackage;
				if (forceDispatch && (mStored[ware] < iPackage))
					quantity = mStored[ware];

				sendSurvivor(survivor, bWarehouse, ware, quantity);

				return true;
			}
		}

		return false;
	}

	/**
	 * Method needed to dispatch goods and survivors to slowly spread resources
	 * over economy network
	 * 
	 * @return true if any dispatch has been made
	 */
	private boolean dispatchEconomyDesired() {
		ArrayList<GameObjectBuilding> others = new ArrayList<GameObjectBuilding>();
		ArrayList<GameObjectBuilding> _others = mBuilding.getNeighborhood();

		/* Set on others array only other warehouses */
		others.clear();
		for (int i = 0; i < _others.size(); i++) {
			if (_others.get(i).properties().canSupplyRequests())
				others.add(_others.get(i));
		}

		if (others.size() < 1)
			return false;

		/* Ensures a little of randomness */
		Collections.shuffle(others, GameWorld.$().getRandom());

		final int[] balanced = GameWorld.$().getEconomyManager()
				.getEconomyOf(mBuilding).getBalanced();

		if (mBuilding.getInsideSurvivorsCount() < 1)
			return false;

		GameObjectSurvivor survivor = mBuilding.getInsideSurvivor();

		for (int ware = Ware.Min; ware < Ware.Size; ware++) {

			final int iPackage = Ware.pkgSize(ware);

			/* Only when balanced level is reached ware is spread */
			if (mStored[ware] < balanced[ware]
					|| mStored[ware] + iPackage <= mDesired[ware])
				continue;

			final int quantity = mStored[ware] > iPackage ? iPackage
					: mStored[ware];

			final int belowLimit = mStored[ware] + mCarried[ware] - quantity;

			for (int i = 0; i < others.size(); i++) {
				final GameObjectBuilding other = others.get(i);
				final Storage storage = other.getStorage();

				final int upperLimit = storage.mStored[ware]
						+ storage.mCarried[ware] + quantity;

				/* Only send when we have more than our neighbor */
				if (belowLimit > upperLimit) {

					/* Send carrier with goods */
					sendSurvivor(survivor, other, ware, quantity);

					return true;
				}
			}
		}

		return false;
	}

	private void sendSurvivor(final GameObjectSurvivor survivor,
			final GameObjectBuilding target, final int ware, final int quantity) {
		if (ware == Ware.survivor) {
			survivor.setHome(target);
			survivor.startState(AI_States.Return);
			mBuilding.uncache(survivor);
			updateRequest(ware);
		} else if (survivor.startState(AI_States.Transport, target)) {
			mBuilding.uncache(survivor);
			survivor.setWare(ware, quantity);
			decreaseStored(ware, quantity);
			mBuilding.getNotifier().notifyUpdated(mBuilding);
			target.getStorage().increaseCarried(ware, quantity);
			updateRequest(ware);
		}
	}

	private void updateRequests() {
		for (int i = 1; i < Ware.Size; i++) {
			mRequests[i].updatePriority();
			updateRequest(i);
		}
	}

	private void updateRequest(int key) {

		int requested = mDesired[key] - mCarried[key] - mStored[key];

		if (key == Ware.survivor)
			requested -= mBuilding.getNonStoredSurvivorsCount();

		if (requested > 0) {
			mRequests[key].initialize(key, requested);
		} else {
			mRequests[key].initialize(key, 0);
		}
	}

	public void increaseStored(WareStorage pWare) {
		increaseStored(pWare.key, pWare.value);
	}

	public void increaseStored(int ware, int pQuantity) {
		mStored[ware] += pQuantity;
		mBuilding.getNotifier().notifyUpdated(mBuilding);
	}

	public void decreaseStored(int key, int pQuantity) {
		mStored[key] -= pQuantity;
		mBuilding.getNotifier().notifyUpdated(mBuilding);
	}

	private void increaseDesired(int ware, int pQuantity) {
		mDesired[ware] += pQuantity;
		mBuilding.getNotifier().notifyUpdated(mBuilding);
	}

	private void decreaseDesired(int ware, int pQuantity) {
		if (mDesired[ware] > pQuantity) {
			mDesired[ware] -= pQuantity;
		} else {
			mDesired[ware] = 0;
		}
		mBuilding.getNotifier().notifyUpdated(mBuilding);
	}

	private void increaseCarried(int key, int pQuantity) {
		mCarried[key] += pQuantity;
		mBuilding.getNotifier().notifyUpdated(mBuilding);
	}

	private void reduceCarried(WareStorage pWare) {
		if (mCarried[pWare.key] > pWare.value) {
			mCarried[pWare.key] -= pWare.value;
		} else {
			mCarried[pWare.key] = 0;
		}
	}

}
