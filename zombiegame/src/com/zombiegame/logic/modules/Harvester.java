package com.zombiegame.logic.modules;

import com.zombiegame.logic.GameWorld;
import com.zombiegame.logic.Ware;
import com.zombiegame.logic.gameobject.GameObjectBuilding;

public class Harvester extends BuildingModule {

	// ========================================================================
	// Constants
	// ========================================================================

	static final String TAG = "ResourceHarvester";

	// ========================================================================
	// Fields
	// ========================================================================

	boolean mGatherEmpty;
	float mGatherStoredWork;
	int[] mGatherWares; // Temporal array

	// ========================================================================
	// Methods
	// ========================================================================
	/*
	 * Creates a resource harvester module for a building
	 * 
	 * @param building
	 */
	public Harvester(final GameObjectBuilding building) {
		super(building);
		mGatherWares = new int[Ware.Size];
		mGatherEmpty = false;
	}

	public boolean isEmpty() {
		return mGatherEmpty;
	}

	// ========================================================================
	// Overloaded methods
	// ========================================================================

	@Override
	protected void onDestroy() {
		mGatherWares = null;
	}

	@Override
	protected void onClean() {
		int cTmp;
		// Leave working wares on building ware pool
		for (int i = 1; i < Ware.Size; i++) {
			if (mGatherWares[i] > 0) {
				cTmp = mGatherWares[i];
				mGatherWares[i] = 0;
				mBuilding.getStorage().increaseStored(i, cTmp);
			}
		}
		mGatherEmpty = false;
	}

	@Override
	protected void onWork(final float pSecondsElapsed) {

		final int cSurvivors = mBuilding.getInsideSurvivorsCount();

		if (cSurvivors > 0) {
			mGatherStoredWork += pSecondsElapsed * cSurvivors;

			// Gather resources
			doHarvest();

			if (isEmpty()) {
				// Emptied resources on current tile, we can freely destroy
				// workshop just when all resources is delivered to a new
				// warehouse

				// Count total wares stored in building
				int cWareCount = 0;

				for (int i = 0; i < Ware.Size; i++) {
					if (i != Ware.survivor)
						cWareCount += mGatherWares[i]
								+ mBuilding.getStorage().getStored(i);
				}

				if (cWareCount == 0) {
					if (GameWorld.$().getResourceCountFromTile(
							mBuilding.getTile()) == 0) {
						if (mBuilding.properties().isDestroyWhenNoResources()) {
							mBuilding.remove();
						}
					}
				}
			}
		}
	}

	// ========================================================================
	// Helper methods
	// ========================================================================

	private void doHarvest() {
		boolean cGathered = false;
		final float workTime = mBuilding.properties().getGatherTime();

		if (workTime <= 0.0f)
			throw new RuntimeException(
					"Harvesting, getGatherTime() cannot be 0.0f");

		while (mGatherStoredWork >= workTime) {
			mGatherStoredWork -= workTime;

			final int ware = GameWorld.$().getRandomResourceFromTile(
					mBuilding.getTile());

			if (ware != Ware.none) {
				mGatherWares[ware]++;
				cGathered = true;
			} else {
				// No more resources on tile
				mGatherEmpty = true;
			}
		}

		if (cGathered || mGatherEmpty) {
			boolean cUpdated = false;
			int cTmp = 0;
			// Prepare goods to be delivered
			for (int i = 1; i < Ware.Size; i++) {
				cTmp = mGatherWares[i];
				mGatherWares[i] = 0;
				mBuilding.getStorage().increaseStored(i, cTmp);
				cUpdated = true;
			}

			if (!cUpdated)
				mBuilding.getNotifier().notifyUpdated(mBuilding);
		}
	}

}
