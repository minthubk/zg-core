package com.zombiegame.logic.modules;

import com.zombiegame.logic.gameobject.GameObjectBuilding;

public abstract class BuildingModule {

	// ========================================================================
	// Fields
	// ========================================================================

	protected GameObjectBuilding mBuilding;

	// ========================================================================
	// Methods
	// ========================================================================

	public BuildingModule(final GameObjectBuilding building) {
		mBuilding = building;
	}

	public final void destroy() {
		clean();
		onDestroy();
		mBuilding = null;
	}

	public final void clean() {
		onClean();
	}

	public final void work(final float pSecondsElapsed) {
		onWork(pSecondsElapsed);
	}

	/**
	 * Triggered when module is destroyed
	 */
	protected abstract void onDestroy();

	/**
	 * Triggered when owner building is reset or cleaned
	 */
	protected abstract void onClean();

	/**
	 * Triggered when owner building is working
	 * 
	 * @param pSecondsElapsed
	 */
	protected abstract void onWork(final float pSecondsElapsed);
}
