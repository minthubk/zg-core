package com.zombiegame.logic.modules;

import com.zombiegame.logic.gameobject.GameObjectBuilding;

/***
 * Possibilities
 * 
 * Standard : Only one hit point (boolean)
 * 
 * Barrier : Hit points equal to WARE_WOOD, every hit removes one ware_wood
 * 
 * Reinforced Barrier: Hit points equal to WARE_WOOD, every second it ignores
 * one hit.
 * 
 * @author raul
 * 
 */

public class HitPointsModule extends BuildingModule {

	// ========================================================================
	// Fields
	// ========================================================================

	private long mLastIgnoredHit;

	// ========================================================================
	// Methods
	// ========================================================================

	public HitPointsModule(GameObjectBuilding pBuilding) {
		super(pBuilding);
		mLastIgnoredHit = System.nanoTime();
	}

	public synchronized void hit() {

		if (mBuilding.isAlive()) {
			long current = System.nanoTime();

			if (mBuilding.properties().canIgnoreHits()) {

				if ((current - mLastIgnoredHit) / 1000 > mBuilding.properties()
						.getIgnoreHitsDelay()) {
					// Ignore hit
					mLastIgnoredHit = current;
					return;
				}

			}

			mBuilding.hurt();

			if (!mBuilding.isAlive()) {
				mBuilding.reset();
			}
		}
	}

	// ========================================================================
	// Override
	// ========================================================================

	@Override
	protected void onClean() {
		// Nothing todo
	}

	@Override
	protected void onDestroy() {
		// Nothing todo
	}

	@Override
	protected void onWork(float pSecondsElapsed) {
		// Nothing to do
	}

}
