package com.zombiegame.logic.events;

import com.zombiegame.logic.gameobject.GameObjectOwned;

public interface GameObjectListener {

	public void notifyRemoved(GameObjectOwned object);

	public void notifyUpdated(GameObjectOwned object);
}
