package com.zombiegame.logic.gameobject;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.zombiegame.logic.GameWorld;
import com.zombiegame.logic.Request;
import com.zombiegame.logic.Ware;
import com.zombiegame.logic.ai.AI_States;
import com.zombiegame.logic.economy.EconomyNode;
import com.zombiegame.logic.modules.Harvester;
import com.zombiegame.logic.modules.Shooter;
import com.zombiegame.logic.modules.Storage;

public class GameObjectBuilding extends GameObjectOwned implements EconomyNode {

	// ========================================================================
	// Private constants
	// ========================================================================

	private static final String TAG = "Building";

	public static final String WAREHOUSE = "warehouse";
	public static final String GATHERHOUSE = "gatherhouse";
	public static final String GUNTOWER = "guntower";

	// ========================================================================
	// Fields
	// ========================================================================

	ArrayList<GameObjectBuilding> mNeighborhood;
	Harvester mHarvester;
	Shooter mShooter;
	Storage mStorage;
	GameObjectBuilding warehouse;
	GameObjectSurvivor mCachedSurvivor;
	ArrayList<GameObjectSurvivor> mSurvivors;
	ArrayList<Request> requests;
	ArrayList<Request> supplies;

	boolean completed;

	// ========================================================================
	// Constructor
	// ========================================================================

	public GameObjectBuilding() {
		requests = new ArrayList<Request>();
		supplies = new ArrayList<Request>();
		mNeighborhood = new ArrayList<GameObjectBuilding>();
		mSurvivors = new ArrayList<GameObjectSurvivor>();
		// Modules should be initialized after
		mStorage = new Storage(this);
		mHarvester = new Harvester(this);
		mShooter = new Shooter(this);
	}

	public void addRequest(Request request) {
		if (request.getSource() != this) {
			Gdx.app.log(TAG, "WARNING: " + this
					+ " I am not source of request!");
			return;
		}

		requests.add(request);
		for (int index = 0; index < mNeighborhood.size(); index++) {
			final GameObjectBuilding b = mNeighborhood.get(index);
			b.onRequest(request);
		}

	}

	private void cancelAllRequests() {

		for (int index = 0; index < requests.size(); index++) {
			Request r = requests.get(index);

			// Only carrier should be notified
			if (r.getCarrier() == null)
				continue;

			// TODO Cancel carrier task
			// r.getCarrier().cancelTask();

			r.setCarrier(null);
		}

	}

	@Override
	public void reset() {

		cancelAllRequests();

		// Removing neighborhood also clear supplies
		while (mNeighborhood.size() > 0) {
			removeNeighbor(mNeighborhood.get(mNeighborhood.size() - 1));
		}

		mSurvivors.clear();
		mShooter.clean();
		mStorage.clean();
		mHarvester.clean();
		mCachedSurvivor = null;
		warehouse = null;
		completed = false;
		super.reset();
	}

	public Harvester getHarverster() {
		return mHarvester;
	}

	/**
	 * Creates an unmanaged Survivor instance just where building is placed.
	 * Attention: This Survivor is NOT on mSurvivors array!!
	 * 
	 * @return the Survivor instance
	 */
	public GameObjectSurvivor getInsideSurvivor() {
		if (mCachedSurvivor == null) {
			if (getStorage().getStored(Ware.survivor) > 0) {
				getStorage().decreaseStored(Ware.survivor, 1);
				GameObjectSurvivor servant = GameWorld.$().createSurvivor(
						getTile(), false);
				servant.setHome(this);
				return servant;
			}
			return null;
		}
		mCachedSurvivor.setHome(this);
		return mCachedSurvivor;
	}

	public int getInsideSurvivorsCount() {
		if (mCachedSurvivor == null)
			return getStorage().getStored(Ware.survivor);
		else
			return 1 + getStorage().getStored(Ware.survivor);
	}

	public ArrayList<GameObjectBuilding> getNeighborhood() {
		return mNeighborhood;
	}

	/**
	 * Get current priority of ware
	 * 
	 * @param key
	 * @return Integer ware priority
	 */
	public int getPriority(int key) {
		final int cExcess = mStorage.getStored(key) - mStorage.getDesired(key)
				- mStorage.getCarried(key);
		if (cExcess > 50)
			return 50;
		if (cExcess < -50)
			return 150;
		return 100 - cExcess;
	}

	public Storage getStorage() {
		return mStorage;
	}

	@Override
	public int getStock(int ware) {
		return mStorage.getStored(ware);
	}

	@Override
	public void setBalanced(int ware, int quantity) {
		/* TODO Not implemented */
		return;
	}

	@Override
	public boolean inRange(EconomyNode node) {
		return inRange(node, true);
	}

	public boolean inRange(EconomyNode node, boolean onlyWarehouses) {

		GameObjectBuilding other = (GameObjectBuilding) node;

		if (onlyWarehouses) {
			if (!properties().canSupplyRequests()
					|| !other.properties().canSupplyRequests())
				return false;
		} else {
			/* Allow at least one non warehouse */
			if (!properties().canSupplyRequests()
					&& !other.properties().canSupplyRequests())
				return false;
		}

		int maxTileRange = Math.max(properties().getSupplyRadius(), other
				.properties().getSupplyRadius());
		float maxRange = maxTileRange
				* GameWorld.$().getMap().getTiled().tileWidth;

		return distance(other) <= maxRange;

	}

	public GameObjectBuilding nearestWarehouse() {
		return warehouse;
	}

	private void searchWarehouse() {
		float wDist = Float.MAX_VALUE;
		float cDist = Float.MAX_VALUE;

		for (int i = 0; i < mNeighborhood.size(); i++) {
			GameObjectBuilding w = mNeighborhood.get(i);

			if (!w.getName().equals(WAREHOUSE))
				continue;

			wDist = distance(w);

			if (wDist > cDist)
				continue;

			warehouse = w;
			cDist = wDist;

		}
	}

	public void setup() {
		if (name.equals(WAREHOUSE)) {
			mStorage.increaseDesiredGoods(Ware.survivor);
			mStorage.increaseDesiredGoods(Ware.wood);
			mStorage.increaseDesiredGoods(Ware.bullet);
		} else if (name.equals(GATHERHOUSE)) {
			mStorage.increaseDesiredGoods(Ware.survivor);
		} else if (name.equals(GUNTOWER)) {
			mStorage.increaseDesiredGoods(Ware.survivor);
			mStorage.increaseDesiredGoods(Ware.wood);
		} else {
			Gdx.app.error(TAG, name + " dont have a 'default required' wares");
		}
	}

	@Override
	public void notifyRemoved(GameObjectOwned object) {

		if (object instanceof GameObjectBuilding) {
			mNeighborhood.remove(object);
			if (warehouse == object) {
				warehouse = null;
				searchWarehouse();
			}
			// TODO May be update requests?
		}
	}

	@Override
	public void notifyUpdated(GameObjectOwned object) {
	}

	/**
	 * Fired when neighbor adds a requests
	 * 
	 * @param request
	 */
	private void onRequest(Request request) {
		if (request == null)
			return;

		if (!supplies.contains(request))
			supplies.add(request);
	}

	@Override
	public float animationDuration() {
		return 1f;
	}

	/**
	 * Fired when neighbor removes a request
	 * 
	 * @param request
	 */
	private void onRequestCanceled(Request request) {
		if (request == null)
			return;
		supplies.remove(request);
	}

	public void registerSurvivor(final GameObjectSurvivor survivor) {
		if (!mSurvivors.contains(survivor))
			mSurvivors.add(survivor);

		notifier.addListener(survivor);
	}

	public boolean isNeighbor(GameObjectBuilding building) {
		return mNeighborhood.contains(building);
	}

	public void addNeighbor(GameObjectBuilding pBuilding) {

		if (isNeighbor(pBuilding))
			return;

		// Link neighbor to prevent infinite recursion
		mNeighborhood.add(pBuilding);

		// Update nearest warehouse
		if (pBuilding.getName().equals(WAREHOUSE)) {
			if (warehouse == null)
				warehouse = pBuilding;
			else if (distance(pBuilding) < distance(warehouse))
				warehouse = pBuilding;
		}

		// Add all own requests to new neighbor
		for (int i = 0; i < requests.size(); i++) {
			final Request req = requests.get(i);
			pBuilding.onRequest(req);
		}

		// Ensures neighborhood of buildings
		pBuilding.addNeighbor(this);

	}

	public void removeNeighbor(GameObjectBuilding building) {
		if (!isNeighbor(building))
			return;

		mNeighborhood.remove(building);

		if (warehouse == building) {
			warehouse = null;
			searchWarehouse();
		}

		// Remove supplies
		for (int index = supplies.size() - 1; index >= 0; index--) {
			Request s = supplies.get(index);
			if (s.getSource() == building)
				supplies.remove(index);
		}
		// Prevents not fully neighborhood clean
		building.removeNeighbor(this);
	}

	public void removeRequest(Request request) {
		if (requests.remove(request)) {
			for (int index = 0; index < mNeighborhood.size(); index++) {
				final GameObjectBuilding b = mNeighborhood.get(index);
				b.onRequestCanceled(request);
			}
			// TODO Recycle request
		}
	}

	public void uncache(GameObjectSurvivor survivor) {
		if (mCachedSurvivor == survivor)
			mCachedSurvivor = null;

		GameWorld.$().manage(survivor);
	}

	/**
	 * Unregister is always safe
	 * 
	 * @param survivor
	 */
	public void unregisterSurvivor(final GameObjectSurvivor survivor) {
		mSurvivors.remove(survivor);
		notifier.removeListener(survivor);
	}

	@Override
	public void hurt() {
		super.hurt();
		if (!isAlive())
			onRemove();
	}

	@Override
	void onRemove() {

		// Drop survivors to map
		GameObjectSurvivor s;
		do {
			s = getInsideSurvivor();

			if (s != null) {
				mCachedSurvivor = null;
				s.setHome(null);
				s.setPosition(position.x, position.y);
				s.getTile();
				GameWorld.$().manage(s);
				s.startState(AI_States.Idle);
			}

		} while (s != null);

		// Drop resources to map
		for (int i = 0; i < Ware.Size; i++) {
			int value = mStorage.getStored(i);
			if (value > 0)
				GameWorld.$().dropResource(getTile(), i, value);
		}
		mStorage.clean();

		// Remove neighborhood
		while (mNeighborhood.size() > 0) {
			final int index = mNeighborhood.size() - 1;
			removeNeighbor(mNeighborhood.get(index));
		}

	}

	@Override
	public void onUpdate(float delta) {

		/* Only completed buildings can do things */
		if (completed) {

			// Acquire idle survivors
			acquireSurvivor();

			// Only harvester, storage and shooter aren't event driven
			if (properties().canGather()) {
				mHarvester.work(delta);
			}

			if (properties().canSupplyRequests()
					|| properties().isMustDispatchGoods()) {
				mStorage.work(delta);
			}

			if (properties().canShot()) {
				mShooter.work(delta);
			}

		} else {
			checkBuildingResources();
			mStorage.work(delta);
		}
	}

	private void checkBuildingResources() {
		if (completed)
			return;

		boolean allowed = true;

		String[] cost = properties().getBuildCost();
		for (int i = 0; i < cost.length; i++) {
			String thisCost = cost[i];
			String key = thisCost.split("_")[0];
			int wareKey = Ware.valueOf(key);
			int value = Integer.parseInt(thisCost.split("_")[1]);

			if (mStorage.getDesired(wareKey) < value)
				mStorage.increaseDesiredGoods(wareKey);

			allowed = allowed && (mStorage.getStored(wareKey) >= value);
		}

		completed = allowed;

	}

	public int getNonStoredSurvivorsCount() {
		return mSurvivors.size();
	}

	private void acquireSurvivor() {
		final ArrayList<GameObjectSurvivor> s_ = (ArrayList<GameObjectSurvivor>) mSurvivors;
		@SuppressWarnings("unchecked")
		final ArrayList<GameObjectSurvivor> sList = (ArrayList<GameObjectSurvivor>) s_
				.clone();

		for (int i = 0; i < sList.size(); i++) {
			final GameObjectSurvivor surv = sList.get(i);

			// We never acquire a cached survivor
			// if (surv == mCachedSurvivor)
			// continue;

			// Ignore survivors of others buildings
			if (surv.getHome() != this)
				continue;

			// Only survivors on same tile can be acquired
			if (surv.getTile() != getTile())
				continue;

			if (surv.currentState == AI_States.Idle) {

				// Remove local references
				if (surv == mCachedSurvivor)
					mCachedSurvivor = null;

				// Unload wares on building
				mStorage.carrierArrived(surv);

				// Add survivor to building
				mStorage.increaseStored(Ware.survivor, 1);

				// Survivor itself is who removes is own references from whole
				// game
				surv.remove();
			}
		}
	}

}
