package com.zombiegame.logic.gameobject;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.zombiegame.util.MathUtils;

public class GameObject {

	// =======================================================================
	// Constants
	// =======================================================================

	private final static String TAG = "GameObject";

	/* Animations */
	public static final int ANIMATION_IDLE = 0;
	public static final int ANIMATION_MOVE = ANIMATION_IDLE + 1;
	public static final int ANIMATION_DIE = ANIMATION_MOVE + 1;
	public static final int ANIMATION_ATTACK = ANIMATION_DIE + 1;

	/* Tweens!!! */
	public static final int TWEEN_MOVE = 1;
	public static final int TWEEN_SIZE = TWEEN_MOVE + 1;

	// Fields
	private static Integer lastSerial = new Integer(0);
	private static StringBuilder strings = new StringBuilder();

	int serial;
	private boolean outOfGame;

	Vector2 position;
	Vector2 size;
	double rotation;
	Rectangle boundingBox;

	int animation;
	float animationTime;
	float animationDuration;

	// Constructor
	public GameObject() {
		position = new Vector2();
		size = new Vector2();
		boundingBox = new Rectangle();
	}

	void acquireSerial() {
		synchronized (lastSerial) {
			lastSerial++;
			serial = lastSerial;
		}
		strings.setLength(0);
		strings.append("Serial: ").append(serial);
		Gdx.app.log(TAG, strings.toString());
	}

	/* Simple collision, pixel perfect needs another approach */
	public boolean collidesWith(GameObject other) {
		float x1, y1, w1, h1;
		float x2, y2, w2, h2;

		x1 = position.x;
		y1 = position.y;
		w1 = size.x;
		h1 = size.y;

		x2 = other.position.x;
		y2 = other.position.y;
		w2 = other.size.x;
		h2 = other.size.y;

		/* TODO Bounding boxes get modified on setPosition and setSize */
		boundingBox.set(x1, y1, w1, h1);
		other.boundingBox.set(x2, y2, w2, h2);

		if (!boundingBox.overlaps(other.boundingBox))
			return false;
		// return true;

		/* Bellow are for pixel perfect approach, incomplete */
		float[] AABB1tl = { x1, y1 + h1 };
		float[] AABB1br = { x1 + w1, y1 };
		float[] AABB2tl = { x2, y2 + h2 };
		float[] AABB2br = { x2 + w2, y2 };

		/* Set origin on AABB1tl */
		AABB2br[0] -= AABB1tl[0];
		AABB2br[1] -= AABB1tl[1];

		AABB2tl[0] -= AABB1tl[0];
		AABB2tl[1] -= AABB1tl[1];

		AABB1br[0] -= AABB1tl[0];
		AABB1br[1] -= AABB1tl[1];

		AABB1tl[0] -= AABB1tl[0];
		AABB1tl[1] -= AABB1tl[1];

		/* TODO Pixel perfect collision */

		return true;
	}

	public float distance(GameObject pOther) {
		return MathUtils.distance(position.x, position.y, pOther.position.x,
				pOther.position.y);
	}

	public boolean hit(float x, float y) {
		return (x > position.x && x < position.x + size.x && y > position.y && y < position.y
				+ size.y);
	}

	public int getAnimation() {
		return animation;
	}

	public float getAnimationTime() {
		return animationTime;
	}

	public Vector2 getPosition() {
		return position;
	}

	public double getRotation() {
		return rotation;
	}

	public Vector2 getSize() {
		return size;
	}

	public boolean isAnimationEnd() {
		return animationTime > animationDuration;
	}

	/**
	 * Return true when object is self-destroying. To prevent updating. To clear
	 * this bit just call GameObject#clean()
	 * 
	 * @return
	 */
	public final boolean isOutOfGame() {
		return outOfGame;
	}

	public void remove() {
		outOfGame = true;
	}

	public void reset() {
		outOfGame = false;
		setPosition(-1f, -1f);
		animation = ANIMATION_IDLE;
	}

	public void setAnimation(int newAnimation) {
		setAnimation(newAnimation, 1f);
	}

	public void setAnimation(int newAnimation, float duration) {
		animation = newAnimation;
		animationTime = 0.0f;
		animationDuration = duration;
	}

	public void setPosition(float x, float y) {
		position.x = x;
		position.y = y;
	}

	public void setSize(float nw, float nh) {
		size.x = nw;
		size.y = nh;
	}

	public void update(float delta) {
		animationTime += delta;
	}

}
