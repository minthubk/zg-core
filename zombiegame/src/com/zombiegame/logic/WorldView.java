package com.zombiegame.logic;

import java.util.ArrayList;
import java.util.List;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.tiled.TileMapRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.zombiegame.logic.events.UIListener;
import com.zombiegame.logic.events.WorldListener;
import com.zombiegame.logic.gameobject.GameObjectBuilding;
import com.zombiegame.logic.gameobject.GameObjectMapResource;
import com.zombiegame.logic.gameobject.GameObjectOwned;
import com.zombiegame.logic.gameobject.GameObjectSurvivor;
import com.zombiegame.logic.gameobject.GameObjectZombie;
import com.zombiegame.ui.BuildingUI;
import com.zombiegame.ui.ConstructionUI;
import com.zombiegame.util.Assets;
import com.zombiegame.util.OrthographicCameraController;

public class WorldView implements WorldListener, UIListener, InputProcessor {

	private static final String TAG = "WorldView";

	GameWorld mGameWorld;
	TileMapRenderer mMapRenderer;
	OrthographicCamera mCamera;
	InputMultiplexer multiplexer;
	OrthographicCameraController mCameraController;
	SpriteBatch mBatch;
	Matrix4 mWorldMatrix;
	Matrix4 mLayoutMatrix;
	Vector3 mTmpVector3;

	ArrayList<BuildingUI> buildingUIs;
	ConstructionUI constructionUI;
	boolean showMenu;

	Sprite ellipse = Assets.ellipse;
	/*
	 * When true, a blended ellipse is drawn over warehouses marking the
	 * influence radius
	 */
	boolean showWarehouseRanges;
	/*
	 * When true, a blended ellipse is drawn over towers marking its influence
	 * radius
	 */
	boolean showTowerRanges;

	// Cached sprite
	Sprite mSprite;

	StringBuilder tmpString = new StringBuilder();

	/* */
	static TweenManager tweenManager;

	// Demo fields
	boolean mDemoMode = false;
	Vector3 camDirection = new Vector3(1, 1, 0);
	Vector2 mMaxCamPosition = new Vector2(0, 0);

	public WorldView(GameWorld pGameWorld, SpriteBatch pSpriteBatch) {

		long start, end;

		start = System.currentTimeMillis();
		mGameWorld = pGameWorld;
		mBatch = pSpriteBatch;

		mLayoutMatrix = mBatch.getProjectionMatrix().cpy();

		multiplexer = new InputMultiplexer();
		multiplexer.getProcessors().ordered = true;

		mCamera = new OrthographicCamera(Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight());

		mTmpVector3 = new Vector3();
		mSprite = new Sprite();
		buildingUIs = new ArrayList<BuildingUI>();
		constructionUI = new ConstructionUI(this);
		/* FIXME Move this line to ZombieGame, and only use Pool on Android */
		Tween.setPoolEnabled(true);
		tweenManager = new TweenManager();

		end = System.currentTimeMillis();

		Gdx.app.log(TAG, "Constructor time " + (end - start) + "ms");
	}

	public void dispose() {
		constructionUI.dispose();
		for (int i = 0; i < buildingUIs.size(); i++) {
			buildingUIs.get(i).dispose();
		}
		buildingUIs.clear();
		buildingUIs = null;
		mWorldMatrix = null;
		mLayoutMatrix = null;
		mGameWorld = null;
		mCamera = null;
		mCameraController = null;
		mBatch = null;
	}

	public void render() {
		mCamera.update();

		if (mDemoMode) {
			updateCamera();
		}

		tweenManager.update();
		renderMap();

		mBatch.setProjectionMatrix(mCamera.combined);

		mBatch.begin();
		renderResources();
		renderZombies();
		renderSurvivors();
		renderBuildings();
		mBatch.end();

		renderUI();
	}

	private void renderResources() {
		/* Now render resources */
		MapResourceManager resources = mGameWorld.getResources();

		for (int i = 0; i < resources.tiles.size(); i++) {
			GameObjectMapResource mrt = resources.tiles.get(i);

			/* TODO Culling */

			if (mrt.hasZombies()) {
				mBatch.draw(Assets.zombieMarker, mrt.getPosition().x,
						mrt.getPosition().y);
			} else if (mrt.resourceCount() > 0) {
				mBatch.draw(
						Assets.scrap.get(mrt.getTile() % Assets.scrap.size()),
						mrt.getPosition().x, mrt.getPosition().y);
			}
		}
	}

	private void renderBuildings() {

		final List<GameObjectBuilding> buildings = mGameWorld.getBuildings();

		for (int i = 0; i < buildings.size(); i++) {
			GameObjectBuilding b = buildings.get(i);
			TextureRegion frame = Assets.buildingSet
					.get(b.getName().hashCode()).getKeyFrame(
							b.getAnimationTime(), true);

			mBatch.draw(frame, b.getPosition().x, b.getPosition().y);

			if (showTowerRanges && b.properties().canShot()) {
				// Show red ellipse
				ellipse.setBounds(
						b.getPosition().x
								- (b.properties().getSupplyRadius() - 1)
								* mMapRenderer.getMap().tileWidth,
						b.getPosition().y
								- (b.properties().getSupplyRadius() - 1)
								* mMapRenderer.getMap().tileHeight, (b
								.properties().getSupplyRadius() - 1)
								* mMapRenderer.getMap().tileWidth * 2, (b
								.properties().getSupplyRadius() - 1)
								* mMapRenderer.getMap().tileHeight * 2);
				ellipse.setColor(Color.RED);
				ellipse.draw(mBatch, 0.155f);

			}

			if (showWarehouseRanges && b.properties().canSupplyRequests()) {
				// Show green ellipse
				ellipse.setBounds(
						b.getPosition().x
								- (b.properties().getSupplyRadius() - 1)
								* mMapRenderer.getMap().tileWidth,
						b.getPosition().y
								- (b.properties().getSupplyRadius() - 1)
								* mMapRenderer.getMap().tileHeight, (b
								.properties().getSupplyRadius() - 1)
								* mMapRenderer.getMap().tileWidth * 2, (b
								.properties().getSupplyRadius() - 1)
								* mMapRenderer.getMap().tileHeight * 2);
				ellipse.setColor(Color.GREEN);
				ellipse.draw(mBatch, 0.15f);

			}
		}

		/* Render BuildingUIs */
		for (int i = 0; i < buildingUIs.size(); i++) {
			BuildingUI b = buildingUIs.get(i);
			b.render(mBatch);
		}

		/* Render ConstructionUI */
		constructionUI.draw(mBatch);

	}

	public Camera getCamera() {
		return mCamera;
	}

	public void addInputProcessor(InputProcessor processor) {
		multiplexer.getProcessors().insert(0, processor);
	}

	public void removeInputProcessor(InputProcessor processor) {
		multiplexer.removeProcessor(processor);
	}

	private void renderMap() {
		mMapRenderer.render(mCamera);
	}

	private void renderSurvivors() {

		final List<GameObjectSurvivor> survivors = mGameWorld.getSurvivors();

		for (int i = 0; i < survivors.size(); i++) {
			GameObjectSurvivor s = survivors.get(i);
			TextureRegion frame;

			switch (s.getAnimation()) {
			case GameObjectOwned.ANIMATION_DIE:
				frame = Assets.survivorSet.die.getKeyFrame(
						s.getAnimationTime(), false);
				break;
			case GameObjectOwned.ANIMATION_ATTACK:
				frame = Assets.survivorSet.attack.getKeyFrame(
						s.getAnimationTime(), false);
				break;
			case GameObjectOwned.ANIMATION_MOVE:
				frame = Assets.survivorSet.walk.getKeyFrame(
						s.getAnimationTime(), true);
				break;
			case GameObjectOwned.ANIMATION_IDLE: // Fallthrought
			default:
				frame = Assets.survivorSet.idle.getKeyFrame(
						s.getAnimationTime(), true);
				break;
			}

			mSprite.setRegion(frame);
			mSprite.setPosition(s.getPosition().x, s.getPosition().y);
			mSprite.setSize(s.getSize().x, s.getSize().y);
			mSprite.setOrigin(mSprite.getWidth() / 2, mSprite.getHeight() / 2);

			mSprite.setRotation((float) (com.badlogic.gdx.math.MathUtils.radiansToDegrees * s
					.getRotation()));

			mSprite.draw(mBatch);

		}

	}

	// This method should be moved outside
	private void renderUI() {

		mBatch.setProjectionMatrix(mLayoutMatrix);
		mBatch.begin();

		Assets.font.draw(mBatch, "FPS: " + Gdx.graphics.getFramesPerSecond(),
				20, 20);

		tmpString.setLength(0);
		Assets.font.draw(
				mBatch,
				tmpString.append("InitialCol, LastCol: ")
						.append(mMapRenderer.getInitialCol()).append(",")
						.append(mMapRenderer.getLastCol()), 20, 40);

		tmpString.setLength(0);
		Assets.font.draw(
				mBatch,
				tmpString.append("InitialRow, LastRow: ")
						.append(mMapRenderer.getInitialRow()).append(",")
						.append(mMapRenderer.getLastRow()), 20, 60);

		mTmpVector3.set(0, 0, 0);
		mCamera.unproject(mTmpVector3);

		tmpString.setLength(0);
		Assets.font.draw(mBatch,
				tmpString.append("Location: ").append(mTmpVector3.x)
						.append(",").append(mTmpVector3.y), 20, 80);
		mBatch.end();
	}

	private void renderZombies() {
		final List<GameObjectZombie> zombies = mGameWorld.getZombies();

		synchronized (zombies) {

			for (int i = 0; i < zombies.size(); i++) {
				final GameObjectZombie z = zombies.get(i);
				TextureRegion frame;

				switch (z.getAnimation()) {
				case GameObjectOwned.ANIMATION_DIE:
					frame = Assets.zombieSet.die.getKeyFrame(
							z.getAnimationTime(), false);
					break;
				case GameObjectOwned.ANIMATION_ATTACK:
					frame = Assets.zombieSet.attack.getKeyFrame(
							z.getAnimationTime(), false);
					break;
				case GameObjectOwned.ANIMATION_MOVE:
					frame = Assets.zombieSet.walk.getKeyFrame(
							z.getAnimationTime(), true);
					break;
				case GameObjectOwned.ANIMATION_IDLE: // Fallthrought
				default:
					frame = Assets.zombieSet.idle.getKeyFrame(
							z.getAnimationTime(), true);
					break;
				}

				mSprite.setRegion(frame);
				mSprite.setPosition(z.getPosition().x, z.getPosition().y);
				mSprite.setSize(z.getSize().x, z.getSize().y);
				mSprite.setOrigin(mSprite.getWidth() / 2,
						mSprite.getHeight() / 2);

				mSprite.setRotation((float) (com.badlogic.gdx.math.MathUtils.radiansToDegrees * z
						.getRotation()));

				mSprite.draw(mBatch);
			}
		}

	}

	private void updateCamera() {
		mCamera.position.add(camDirection.tmp()
				.mul(Gdx.graphics.getDeltaTime())
				.mul(5 * mMapRenderer.getUnitsPerTileX()));

		if (mCamera.position.x < 0) {
			mCamera.position.x = 0;
			camDirection.x = 1;
		}
		if (mCamera.position.x > mMaxCamPosition.x) {
			mCamera.position.x = mMaxCamPosition.x;
			camDirection.x = -1;
		}
		if (mCamera.position.y < 0) {
			mCamera.position.y = 0;
			camDirection.y = 1;
		}
		if (mCamera.position.y > mMaxCamPosition.y) {
			mCamera.position.y = mMaxCamPosition.y;
			camDirection.y = -1;
		}
	}

	public boolean isShowMenu() {
		return showMenu;
	}

	@Override
	public void onMapLoaded() {
		// Load map assets
		Assets.loadMapAtlas(mGameWorld.getMap().getTiled());

		// Map is loaded by GameWorld but tiles are loaded here
		try {
			long startTime, endTime;
			startTime = System.currentTimeMillis();
			mMapRenderer = new TileMapRenderer(mGameWorld.getMap().getTiled(),
					Assets.getMapAtlas(),
					mGameWorld.getMap().getTiled().tileWidth, mGameWorld
							.getMap().getTiled().tileHeight, mGameWorld
							.getMap().getTiled().tileWidth, mGameWorld.getMap()
							.getTiled().tileHeight);
			endTime = System.currentTimeMillis();
			Gdx.app.log(TAG, "Cache created in " + (endTime - startTime) + "mS");
		} catch (Exception e) {
			Gdx.app.log(TAG, e.toString());
		}

		mCamera.position.set(mMapRenderer.getMapWidthUnits() / 2,
				mMapRenderer.getMapHeightUnits() / 2, 0);

		mCameraController = new OrthographicCameraController(mCamera,
				mMapRenderer.getMapWidthUnits(),
				mMapRenderer.getMapHeightUnits());
		mCameraController.getNotifier().addListener(this);

		multiplexer.clear();
		multiplexer.addProcessor(mCameraController);
		multiplexer.addProcessor(this); // Only for back pressed

		mMaxCamPosition.set(mMapRenderer.getMapWidthUnits(),
				mMapRenderer.getMapHeightUnits());

	}

	@Override
	public void onGameObjectPlaced(GameObjectOwned object) {
	}

	@Override
	public void onGameObjectRemoved(GameObjectOwned object) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGameObjectStartDie(GameObjectOwned object) {
		// TODO Auto-generated method stub
		// Tween.to(object, GameObject.TWEEN_SIZE, 5000, Bounce.INOUT)
		// .target(0, 0).addToManager(tweenManager);
	}

	@Override
	public void tap(Vector3 position) {
		boolean hided = false;
		if (constructionUI.isVisible()) {
			constructionUI.setVisible(false);
			hided = true;
		}

		Gdx.app.log(TAG,
				"Tile: " + mGameWorld.worldToTile(position.x, position.y));

		/* Here, single tap. Currently only to select buildings */
		final GameObjectOwned object = mGameWorld.getGameObjectAt(position);
		if (object == null) {
			if (!hided) {
				constructionUI.setup(position.x, position.y);
				constructionUI.setVisible(true);
			}
			return;
		}

		/* Select object */
		if (object instanceof GameObjectBuilding) {

			/* Get a building ui */
			final GameObjectBuilding building = (GameObjectBuilding) object;
			final BuildingUI bui = getBuildingUI(building);
			bui.register(building);
			bui.setVisible(true);
			if (!buildingUIs.contains(bui))
				buildingUIs.add(bui);
			return;
		}
	}

	@Override
	public void doubleTap(Vector3 position) {
		// TODO Auto-generated method stub
	}

	@Override
	public void holdTap(Vector3 position) {
	}

	private BuildingUI getBuildingUI(GameObjectBuilding building) {

		BuildingUI bUI = null;
		for (int i = 0; i < buildingUIs.size(); i++) {
			bUI = buildingUIs.get(i);
			if (!bUI.isVisible() || (bUI.getBuilding() == building)) {
				return bUI;
			}
		}

		bUI = new BuildingUI(mGameWorld, this);
		return bUI;
	}

	public void buildAt(int type, Vector3 root) {
		int tile = mGameWorld.worldToTile(root.x, root.y)
				+ mGameWorld.getMap().getTiled().width;

		switch (type) {
		case 0:
			mGameWorld.createBuilding(tile, GameObjectBuilding.WAREHOUSE);
			break;
		case 1:
			mGameWorld.createBuilding(tile, GameObjectBuilding.GATHERHOUSE);
			break;
		case 2:
			mGameWorld.createBuilding(tile, GameObjectBuilding.GUNTOWER);
			break;
		}

	}

	public GameWorld getWorld() {
		return mGameWorld;
	}

	@Override
	public void onGameObjectReserveMapPosition(GameObjectOwned go, int col,
			int row) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGameObjectLeaveMapPosition(GameObjectOwned go, int col,
			int row) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onPaused() {
		// TODO Auto-generated method stub
		Gdx.app.log(TAG, "onPaused");
	}

	@Override
	public void onResumed() {
		Gdx.input.setInputProcessor(multiplexer);
		Gdx.input.setCatchBackKey(true);
	}

	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Keys.BACK || keycode == Keys.MENU
				|| keycode == Keys.ESCAPE) {
			showMenu = true;
			mGameWorld.pause();
			return true;
		} else if (keycode == Keys.SPACE) {
			showMenu = false;
			if (!GameWorld.$().isPaused()) {
				GameWorld.$().pause();
			} else {
				GameWorld.$().resume();
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int x, int y, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchMoved(int x, int y) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
