package com.zombiegame.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.FadeTo;
import com.badlogic.gdx.scenes.scene2d.actions.Forever;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.zombiegame.ZombieGame;
import com.zombiegame.ui.SimpleButton;
import com.zombiegame.util.Assets;

public class OptionsScreen implements Screen, InputProcessor {

	// private final static String TAG = "OptionsScreen";

	Stage stage;
	Image bGround;
	SpriteBatch batch;
	Sprite background;
	SimpleButton back;
	SimpleButton musicUp;
	SimpleButton musicDown;
	SimpleButton sfxUp;
	SimpleButton sfxDown;

	String musicLabel;
	float musicValue;

	String sfxLabel;
	float sfxValue;

	public OptionsScreen() {
		// Assets.loadOptions();
		batch = new SpriteBatch();
		stage = new Stage(ZombieGame.WIDTH, ZombieGame.HEIGHT, true);

		bGround = new Image(Assets.optionsBackgound);
		stage.addActor(bGround);

		back = new SimpleButton("Back", Assets.menuFont);

		musicLabel = "Music volume";
		musicDown = new SimpleButton("Low", Assets.menuFont);
		musicUp = new SimpleButton("High", Assets.menuFont);
		musicDown.topOn(225);
		musicDown.rightOn(450);
		musicUp.topOn(225);
		musicUp.rightOn(700);

		sfxLabel = "Sound volume";
		sfxDown = new SimpleButton("Low", Assets.menuFont);
		sfxUp = new SimpleButton("High", Assets.menuFont);
		sfxDown.topOn(325);
		sfxDown.rightOn(450);
		sfxUp.topOn(325);
		sfxUp.rightOn(700);

		stage.getRoot()
				.action(Forever.$(Sequence.$(FadeTo.$(0.3f, 0.2f),
						FadeTo.$(0.8f, 0.3f))));
	}

	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Keys.BACK || keycode == Keys.ESCAPE) {
			ZombieGame.$().showMenu();
			return true;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int x, int y, int pointer) {
		return false;
	}

	@Override
	public boolean touchMoved(int x, int y) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		updateButtons(delta);
		stage.act(delta);

		if (musicDown.wasPressed()) {
			musicValue = (musicValue < 0.1f) ? 0f : musicValue - 0.1f;
			Assets.musicMenu.setVolume(musicValue);
		} else if (musicUp.wasPressed()) {
			musicValue = (musicValue > 0.9f) ? 1f : musicValue + 0.1f;
			Assets.musicMenu.setVolume(musicValue);
		} else if (sfxDown.wasPressed()) {
			sfxValue = (sfxValue < 0.1f) ? 0f : sfxValue - 0.1f;
		} else if (sfxUp.wasPressed()) {
			sfxValue = (sfxValue > 0.9f) ? 1f : sfxValue + 0.1f;
		} else if (back.wasPressed()) {
			/* Save preferences */
			ZombieGame.$().config()
					.putFloat(ZombieGame.MusicVolume, musicValue);
			ZombieGame.$().config().putFloat(ZombieGame.SfxVolume, sfxValue);
			ZombieGame.$().showMenu();
		}

		/* Draw */
		stage.draw();
		batch.begin();

		sfxDown.draw(batch);
		sfxUp.draw(batch);

		musicDown.draw(batch);
		musicUp.draw(batch);

		Assets.menuFont.draw(batch, sfxLabel, 100, 300);
		Assets.menuFont.draw(batch, "" + Math.round(sfxValue * 10), 500, 300);

		Assets.menuFont.draw(batch, "" + Math.round(musicValue * 10), 500, 200);
		Assets.menuFont.draw(batch, musicLabel, 100, 200);

		back.draw(batch);
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
	}

	@Override
	public void show() {
		musicValue = ZombieGame.$().config()
				.getFloat(ZombieGame.MusicVolume, 0.6f);
		sfxValue = ZombieGame.$().config().getFloat(ZombieGame.SfxVolume, 0.6f);
		if (musicValue > 0.0f) {
			Assets.musicMenu.setVolume(musicValue);
			Assets.musicMenu.setLooping(true);
			Assets.musicMenu.play();
		}
		Gdx.input.setInputProcessor(this);
	}

	@Override
	public void hide() {
		Assets.musicMenu.stop();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
	}

	@Override
	public void dispose() {
		batch.dispose();
	}

	Vector3 touchPoint = new Vector3();
	boolean wasTouched;

	private void updateButtons(float delta) {
		touchPoint = screenToViewport(Gdx.input.getX(), Gdx.input.getY());
		boolean justTouched = Gdx.input.justTouched();
		boolean isTouched = Gdx.input.isTouched();
		boolean justReleased = wasTouched && !isTouched;
		wasTouched = isTouched;
		musicDown.update(delta, justTouched, isTouched, justReleased,
				touchPoint.x, touchPoint.y);
		musicUp.update(delta, justTouched, isTouched, justReleased,
				touchPoint.x, touchPoint.y);
		sfxDown.update(delta, justTouched, isTouched, justReleased,
				touchPoint.x, touchPoint.y);
		sfxUp.update(delta, justTouched, isTouched, justReleased, touchPoint.x,
				touchPoint.y);
		back.update(delta, justTouched, isTouched, justReleased, touchPoint.x,
				touchPoint.y);
	}

	Vector3 screenToViewport(float x, float y) {
		stage.getCamera().unproject(touchPoint.set(x, y, 0));
		return touchPoint;
	}

}
