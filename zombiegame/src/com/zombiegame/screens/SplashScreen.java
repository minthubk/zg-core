package com.zombiegame.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.zombiegame.ZombieGame;

public class SplashScreen implements Screen {

	private ZombieGame mGame;
	private SpriteBatch spriteBatch;
	private Texture mSplash;

	public SplashScreen(ZombieGame pGame) {
		this.mGame = pGame;
	}

	@Override
	public void dispose() {
		mSplash.dispose();
		spriteBatch.dispose();
	}

	@Override
	public void hide() {
		// Auto-generated method stub
		Gdx.app.log("Splash", "hide");
	}

	@Override
	public void pause() {
		// Auto-generated method stub
		Gdx.app.log("Splash", "pause");
	}

	@Override
	public void render(float delta) {

		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		spriteBatch.begin();
		spriteBatch.draw(mSplash,
				(Gdx.graphics.getWidth() - mSplash.getWidth()) * 0.5f,
				(Gdx.graphics.getHeight() - mSplash.getHeight()) * 0.5f);
		spriteBatch.end();

		if (Gdx.input.isTouched()) {
			Gdx.app.log("SplashScreen", "Touched!");
			mGame.showMenu();
		}
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		Gdx.app.log("Splash", "resume");
	}

	@Override
	public void show() {
		Gdx.app.log("Splash", "show");
		spriteBatch = new SpriteBatch();
		mSplash = new Texture(Gdx.files.internal("data/gfx/logo.png"));
	}

}
