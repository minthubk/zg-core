package com.zombiegame.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Delay;
import com.badlogic.gdx.scenes.scene2d.actions.FadeTo;
import com.badlogic.gdx.scenes.scene2d.actions.Forever;
import com.badlogic.gdx.scenes.scene2d.actions.MoveBy;
import com.badlogic.gdx.scenes.scene2d.actions.Parallel;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.zombiegame.ZombieGame;
import com.zombiegame.ui.SimpleButton;
import com.zombiegame.util.Assets;

public class MainMenuScreen implements Screen, InputProcessor {

	private static final String TAG = "MainMenuScreen";

	ZombieGame mGame;
	private SpriteBatch batch;

	private Stage mStage;

	private Image background;

	SimpleButton startGameButton;
	SimpleButton resumeGameButton;
	SimpleButton optionsButton;
	SimpleButton exitGameButton;
	boolean showResumeGame = false;

	GameScreen gameScreen;

	public MainMenuScreen(ZombieGame zombieGame) {
		this.mGame = zombieGame;

		Assets.loadMenu();

		batch = new SpriteBatch();

		mStage = new Stage(ZombieGame.WIDTH, ZombieGame.HEIGHT, true);

		background = new Image(Assets.mainBackgound);

		mStage.addActor(background);

		resumeGameButton = new SimpleButton("Resume game", Assets.menuFont);
		resumeGameButton.centerHorizontallyOn(Gdx.graphics.getWidth() * 0.5f);
		resumeGameButton.topOn(Gdx.graphics.getHeight() * 0.85f);

		startGameButton = new SimpleButton("Start Game", Assets.menuFont);
		startGameButton.centerHorizontallyOn(Gdx.graphics.getWidth() * 0.5f);
		startGameButton.topOn(Gdx.graphics.getHeight() * 0.65f);

		optionsButton = new SimpleButton("Options", Assets.menuFont);
		optionsButton.centerHorizontallyOn(Gdx.graphics.getWidth() * 0.5f);
		optionsButton.topOn(Gdx.graphics.getHeight() * 0.45f);

		exitGameButton = new SimpleButton("Exit", Assets.menuFont);
		exitGameButton.centerHorizontallyOn(Gdx.graphics.getWidth() * 0.5f);
		exitGameButton.topOn(Gdx.graphics.getHeight() * 0.25f);

		Action flas = Delay.$(
				Sequence.$(FadeTo.$(0.1f, 0.1f), FadeTo.$(1f, 0.1f),
						FadeTo.$(0.2f, 0.1f), FadeTo.$(0.9f, 1)), 1.8f);

		Action mov = Delay.$(
				Sequence.$(MoveBy.$(5, 0, 0.1f), MoveBy.$(-10, 0, 0.1f),
						MoveBy.$(5, 0, 0.1f)), 2);

		mStage.getRoot().action(Parallel.$(Forever.$(flas), Forever.$(mov)));

	}

	@Override
	public void dispose() {
		batch.dispose();
	}

	@Override
	public void hide() {
		// Called when current screen changes to this to a different screen
		Gdx.app.log(TAG, "Hiding");
		Assets.musicMenu.stop();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		Gdx.app.log(TAG, "Paused");
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		mStage.act(delta);
		updateButtons(delta);

		mStage.draw();

		if (startGameButton.wasPressed()) {
			startNewGame();
		} else if (resumeGameButton.wasPressed()) {
			resumeGame();
		} else if (exitGameButton.wasPressed()) {
			Gdx.app.exit();
		} else if (optionsButton.wasPressed()) {
			Gdx.app.log(TAG, "Options pressed");
			showOptions();
		} else {
			batch.begin();
			if (showResumeGame)
				resumeGameButton.draw(batch);
			startGameButton.draw(batch);
			optionsButton.draw(batch);
			exitGameButton.draw(batch);
			batch.end();
		}
	}

	Vector3 touchPoint = new Vector3();
	boolean wasTouched;

	private void updateButtons(float delta) {
		touchPoint = screenToViewport(Gdx.input.getX(), Gdx.input.getY());
		boolean justTouched = Gdx.input.justTouched();
		boolean isTouched = Gdx.input.isTouched();
		boolean justReleased = wasTouched && !isTouched;
		wasTouched = isTouched;
		startGameButton.update(delta, justTouched, isTouched, justReleased,
				touchPoint.x, touchPoint.y);
		optionsButton.update(delta, justTouched, isTouched, justReleased,
				touchPoint.x, touchPoint.y);
		exitGameButton.update(delta, justTouched, isTouched, justReleased,
				touchPoint.x, touchPoint.y);
		if (showResumeGame)
			resumeGameButton.update(delta, justTouched, isTouched,
					justReleased, touchPoint.x, touchPoint.y);
	}

	Vector3 screenToViewport(float x, float y) {
		mStage.getCamera().unproject(touchPoint.set(x, y, 0));
		return touchPoint;
	}

	void startNewGame() {
		if (gameScreen == null)
			gameScreen = new GameScreen(mGame);
		gameScreen.resetGame();
		gameScreen.initMap("test2");
		mGame.setScreen(gameScreen);
	}

	void showOptions() {
		ZombieGame.$().showOptions();
	}

	void resumeGame() {
		mGame.setScreen(gameScreen);
	}

	@Override
	public void resize(int width, int height) {
		startGameButton.resize();
		resumeGameButton.resize();
		optionsButton.resize();
		exitGameButton.resize();
		// TODO Auto-generated method stub
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		Gdx.app.log("MainMenu", "Resume");
	}

	@Override
	public void show() {
		// Called when screen is set via game.setScreen
		Gdx.app.log(TAG, "Showing");
		showResumeGame = (gameScreen != null && gameScreen.isGameStarted() && !gameScreen
				.isGameEnded());
		Gdx.input.setInputProcessor(this);

		float volume = ZombieGame.$().config()
				.getFloat(ZombieGame.MusicVolume, 0.7f);

		if (volume > 0.0f) {
			Assets.musicMenu.setLooping(true);
			Assets.musicMenu.setVolume(volume);
			Assets.musicMenu.play();
		}
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		Gdx.app.log(TAG, "Down");
		return mStage.touchDown(x, y, pointer, button);
	}

	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
		return mStage.touchUp(x, y, pointer, button);
	}

	@Override
	public boolean touchDragged(int x, int y, int pointer) {
		return mStage.touchDragged(x, y, pointer);
	}

	@Override
	public boolean touchMoved(int x, int y) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
