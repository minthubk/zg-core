package com.zombiegame.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.zombiegame.ZombieGame;
import com.zombiegame.logic.GameWorld;
import com.zombiegame.logic.WorldView;
import com.zombiegame.util.Assets;

public class GameScreen implements Screen {

	ZombieGame mGame;
	SpriteBatch mSpriteBatch;

	GameWorld mWorld;
	WorldView mWorldRenderer;

	public GameScreen(ZombieGame zombieGame) {
		this.mGame = zombieGame;
		this.mSpriteBatch = new SpriteBatch();

		Assets.load();

		// Creation of world and global listeners
		mWorld = new GameWorld(mGame.gameReporter);
		mWorldRenderer = new WorldView(mWorld, mSpriteBatch);

		// Let listeners listen
		mWorld.addWorldListener(mWorldRenderer);

	}

	public boolean isGameStarted() {
		return mWorld.isMapLoaded();
	}

	public boolean isGameEnded() {
		return mWorld.isEnded();
	}

	public void resetGame() {
		mWorld.reset();
	}

	public void initMap(String mapname) {
		// Load map
		mWorld.loadMap(mapname);
		mWorld.generateTestData();
	}

	@Override
	public void dispose() {
		this.mGame = null;
		mSpriteBatch.dispose();
	}

	@Override
	public void hide() {
		// Save here
		Gdx.app.log("GameScreen", "Hiding");
		mWorld.pause();
	}

	@Override
	public void pause() {
		Gdx.app.log("GameScreen", "Pausing");
	}

	@Override
	public void render(float delta) {

		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		mWorld.update(delta);

		mWorldRenderer.render();

		if (mWorld.isEnded()) {
			mGame.showMenu();
		}

		if (mWorld.isPaused() && mWorldRenderer.isShowMenu()) {
			mGame.showMenu();
		}
	}

	@Override
	public void resize(int width, int height) {
		// Auto-generated method stub
	}

	@Override
	public void resume() {
		// Auto-generated method stub
		Gdx.app.log("GameScreen", "Resuming");
		mWorld.resume();
	}

	@Override
	public void show() {
		// Load saved data here
		Gdx.app.log("GameScreen", "Showing");
		if (mWorld.isPaused())
			mWorld.resume();
		else
			mWorld.start();
	}

}
